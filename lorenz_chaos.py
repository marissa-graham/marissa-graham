import numpy as np
import random
from mayavi import mlab
from scipy import linalg as la
from scipy.integrate import ode
from scipy.integrate import odeint
from scipy.stats import linregress
from matplotlib import pyplot as plt

def prob1():
    # Produce the plot requested in the lab.
    sigma = 10.
    beta = 8/3.
    rho = 28.
    def lorenz_system(y, t):
        return np.array([sigma*(y[1]-y[0]),rho*y[0]-y[1]-y[0]*y[2], y[0]*y[1] - beta*y[2]])
    
    x = np.linspace(0,10,1000)
    def rand_initial():
        return random.randint(-15,15)
    
    for i in xrange(20):
        initial = (rand_initial(), rand_initial(), rand_initial())
        sys = odeint(lorenz_system, initial, x)
        X = sys[:,0]
        Y = sys[:,1]
        Z = sys[:,2]
        mlab.plot3d(X, Y, Z,color=(random.random(),random.random(),random.random()),tube_radius=.25)
    mlab.show()

def prob2(m,T,res,step,delay):
    # Produce the plot requested in the lab.
    # 
    # m = number of trajectories to plot,
    # T = final time value
    # res = resolution for the plot
    # step = stepping number = number of new points to include
    # at each update of the plot
    # delay = time delay to use between iterations
    sigma = 10.
    beta = 8/3.
    rho = 28.
    def lorenz_system(y, t):
        return np.array([sigma*(y[1]-y[0]),rho*y[0]-y[1]-y[0]*y[2], y[0]*y[1] - beta*y[2]])
    
    x = np.linspace(0,T,res)
    def rand_initial():
        return random.randint(-15,15)
    def color():
        return (random.random(),random.random(),random.random())
    X = np.empty((m,res))
    Y = np.empty((m,res))
    Z = np.empty((m,res))
    c = np.array([])
    
    for i in xrange(m):
        initial = (rand_initial(), rand_initial(), rand_initial())
        sys = odeint(lorenz_system, initial, x)
        X[i] = sys[:,0]
        Y[i] = sys[:,1]
        Z[i] = sys[:,2]
        #mlab.plot3d(X, Y, Z,color=(random.random(),random.random(),random.random()),tube_radius=.25)
        c = np.append(c,mlab.plot3d(X[i],Y[i],Z[i],line_width=.2,color=color(),tube_radius=.25))

    @mlab.show
    @mlab.animate(delay=delay)
    def animate():
        scale = 0.
        for j in xrange(2+step,X.size,step):
            for i in xrange(len(c)):
                c[i].mlab_source.reset(x=X[i][:j],y=Y[i][:j],z=Z[i][:j])
                mlab.gcf().scene.reset_zoom()
                yield
    animate()

def prob3(T,res,step,delay):
    # Produce the plot requested in the lab.
    # 
    # T, res, step, and delay are defined as in prob2

    sigma = 10.
    beta = 8/3.
    rho = 28.
    def lorenz_system(y, t):
        return np.array([sigma*(y[1]-y[0]),rho*y[0]-y[1]-y[0]*y[2], y[0]*y[1] - beta*y[2]])
    
    x = np.linspace(0,T,res)
    def rand_initial():
        return random.randint(-15,15)
    def color():
        return (random.random(),random.random(),random.random())
    X = np.empty((2,res))
    Y = np.empty((2,res))
    Z = np.empty((2,res))
    c = []
    
    initial = (rand_initial(), rand_initial(), rand_initial())
    sys = odeint(lorenz_system, initial, x,atol=1e-14,rtol=1e-12)
    X[0] = sys[:,0]
    Y[0] = sys[:,1]
    Z[0] = sys[:,2]
    #mlab.plot3d(X, Y, Z,color=(random.random(),random.random(),random.random()),tube_radius=.25)
    c.append(mlab.plot3d(X[0],Y[0],Z[0],tube_radius=.2,color=color()))

    sys = odeint(lorenz_system, initial, x,atol=1e-15,rtol=1e-13)
    X[1] = sys[:,0]
    Y[1] = sys[:,1]
    Z[1] = sys[:,2]
    #mlab.plot3d(X, Y, Z,color=(random.random(),random.random(),random.random()),tube_radius=.25)
    c.append(mlab.plot3d(X[1],Y[1],Z[1],tube_radius=.2,color=color()))

    
    @mlab.show
    @mlab.animate(delay=delay)
    def animate():
        scale = 0.
        for i in xrange(2+step,X.size,step):
            for j in xrange(2):
                c[j].mlab_source.reset(x=X[j][:i],y=Y[j][:i],z=Z[j][:i])
                mlab.gcf().scene.reset_zoom()
            yield
    animate()


def prob4(T,res,step,delay):
    # Produce the plot requested in the lab.
    # 
    # T, res, step, and delay are defined as in prob2
    #
    # Make sure that the ode solver uses stringent values for 
    # the absolute and relative errors: atol= 1e-15, rtol=1e-13

    sigma = 10.
    beta = 8/3.
    rho = 28.
    def lorenz_system(y, t):
        return np.array([sigma*(y[1]-y[0]),rho*y[0]-y[1]-y[0]*y[2], y[0]*y[1] - beta*y[2]])
    
    x = np.linspace(0,T,res)
    def rand_initial():
        return random.randint(-15,15)
    def color():
        return (random.random(),random.random(),random.random())
    X = np.empty((2,res))
    Y = np.empty((2,res))
    Z = np.empty((2,res))
    c = []
    
    initial = np.random.rand(3)*30-15
    sys = odeint(lorenz_system, initial, x,atol=1e-15,rtol=1e-13)
    X[0] = sys[:,0]
    Y[0] = sys[:,1]
    Z[0] = sys[:,2]
    #mlab.plot3d(X, Y, Z,color=(random.random(),random.random(),random.random()),tube_radius=.25)
    c.append(mlab.plot3d(X[0],Y[0],Z[0],tube_radius=.2,color=color()))

    initial = initial*(1+2.22e-16)
    sys = odeint(lorenz_system, initial, x,atol=1e-15,rtol=1e-13)
    X[1] = sys[:,0]
    Y[1] = sys[:,1]
    Z[1] = sys[:,2]
    #mlab.plot3d(X, Y, Z,color=(random.random(),random.random(),random.random()),tube_radius=.25)
    c.append(mlab.plot3d(X[1],Y[1],Z[1],tube_radius=.2,color=color()))

    
    @mlab.show
    @mlab.animate(delay=delay)
    def animate():
        scale = 0.
        for i in xrange(2+step,X.size,step):
            for j in xrange(2):
                c[j].mlab_source.reset(x=X[j][:i],y=Y[j][:i],z=Z[j][:i])
                mlab.gcf().scene.reset_zoom()
            yield
    animate()

def prob5():
    # Produce the plot requested in the lab.
    # 
    # calculate and return your approximation for the Lyapunov 
    # exponent
    sigma = 10.
    beta = 8/3.
    rho = 28.
    def lorenz(y, t):
        return np.array([sigma*(y[1]-y[0]),rho*y[0]-y[1]-y[0]*y[2], y[0]*y[1] - beta*y[2]])
    
    x = np.linspace(0,10,10000)
    initials = np.random.rand(3)*30-15
    sol = odeint(lorenz, initials, x, atol=1e-15,rtol=1e-13)
    
    initials1 = sol[-1]
    initials2 = initials1*(1+2.22e-16)
    
    sol1 = odeint(lorenz, initials1, x, atol=1e-15, rtol=1e-13)
    sol2 = odeint(lorenz, initials2, x, atol=1e-15, rtol=1e-13)
    
    diff = np.sqrt((sol1-sol2)**2)
    
    plt.semilogy(x, diff.sum(axis=1))
    slope, intercept, r, p, std_err = linregress(x,np.log(diff.sum(axis=1)))
    
    plt.semilogy(x, np.exp(slope*x+intercept))
    plt.show()
    return slope