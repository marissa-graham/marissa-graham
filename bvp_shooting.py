from __future__ import division
import numpy as np
from scipy.integrate import odeint
from matplotlib import pyplot as plt

def find_t(f,a,b,alpha,beta,t0,t1,maxI,v0=None):
    sol1 = np.inf
    i=0
    while abs(sol1-beta) > 10**-8 and i < maxI:
        if v0:
            #print "entered the find_t while loop"
            sol0 = odeint(f,np.array([alpha,v0,t0]), [a,b],atol=1e-7)[1,0]
            sol1 = odeint(f,np.array([alpha,v0,t1]), [a,b],atol=1e-7)[1,0]
            t2 = t1 - (sol1 - beta)*(t1-t0)/(sol1-sol0)
            t0 = t1
            t1 = t2
            i = i+1
        else:
            #print "entered the find_t while loop"
            sol0 = odeint(f,np.array([alpha,t0]), [a,b],atol=1e-7)[1,0]
            sol1 = odeint(f,np.array([alpha,t1]), [a,b],atol=1e-7)[1,0]
            t2 = t1 - (sol1 - beta)*(t1-t0)/(sol1-sol0)
            t0 = t1
            t1 = t2
            i = i+1
    if i == maxI:
        print "t not found"
    return t2

def solveSecant(f,X,a,b,alpha,beta,t0,t1,maxI,v0=None):
    t = find_t(f,a,b,alpha,beta,t0,t1,maxI,v0)
    if v0:
        sol = odeint(f,np.array([alpha,v0,t]), X,atol=1e-7)[:,0]
    else:
        sol = odeint(f,np.array([alpha,t]), X,atol=1e-7)[:,0]
    return sol

def ode1(y,x):
    return np.array([y[1], -4*y[0]-9*np.sin(x)])

def ode2(y,x):
    return np.array([y[1], 3+2*y[0]/x**2])

def prob1():
    # Using the shooting method, find two distinct numerical solutions 
    # for the boundary value problem.
    # y'' = -4*y - 9*sin(x), x in [0,pi], y(0) = 1, y(pi) = 1
    t0 = 1.0
    t1 = 0.0
    
    X = np.linspace(0,np.pi,100)
    Y = solveSecant(ode1,X,0,np.pi,1,1,t0,t1,40)
    plt.plot(X,Y,'-k',linewidth=2)
    
    t0 = np.pi
    t1 = 100.0
    
    X = np.linspace(0,np.pi,100)
    Y = solveSecant(ode1,X,0,np.pi,1,1,t0,t1,40)
    plt.plot(X,Y,'-k',linewidth=2)
    
    plt.show()
    # Plot and show your solutions. 
    
def prob2():
    # Using the shooting method, numerically approximate the solution
    # of the boundary value problem.
    
    t0 = 1.0
    t1 = 0.
    
    X = np.linspace(1,np.e,100)
    Y = solveSecant(ode2,X,1,np.e,6,(np.e)**2 + 6.0/np.e,t0,t1,40)
    plt.plot(X,Y,'-k',linewidth=2)
    plt.show()

def ode3(y,x):
    g = 9.8067
    mu = 0.0003
    return np.array([np.tan(y[2]), -1*(g*np.sin(y[2]) + mu*y[1]**2)/(y[1]*np.cos(y[2])), -g/y[1]**2])

def ode4(y,x):
    g = 9.8067
    mu = 0.0
    return np.array([np.tan(y[2]), -1*(g*np.sin(y[2]) + mu*y[1]**2)/(y[1]*np.cos(y[2])), -g/y[1]**2])

def prob3(n):
    # Using the shooting method, find and return a numerical approximation
    # of the solution to the cannon problem. 
    
    # n+1 = number of grid points for an approximation
    # domain = np.linspace(0,195,n+1)
    #y, v, theta = approximation of solution at gridpoints in domain. 
    #y, v, theta should have shape (n+1,)
    t0 = np.pi/3
    t1 = np.pi/2
    
    X = np.linspace(0,195,n+1)
    Y = solveSecant(ode4,X,0,195,0,0,t0,t1,n+1,v0=45.0)
    plt.plot(X,Y,'-k',linewidth=2,color='b')
    
    t0 = np.pi/8
    t1 = np.pi/6
    
    X = np.linspace(0,195,n+1)
    Y = solveSecant(ode4,X,0,195,0,0,t0,t1,n+1,v0=45.0)
    plt.plot(X,Y,'-k',linewidth=2,color='b')
    
    t0 = np.pi/3
    t1 = np.pi/2
    
    X = np.linspace(0,195,n+1)
    Y = solveSecant(ode3,X,0,195,0,0,t0,t1,n+1,v0=45.0)
    plt.plot(X,Y,'-k',linewidth=2)
    
    t0 = np.pi/8
    t1 = np.pi/6
    
    X = np.linspace(0,195,n+1)
    Y = solveSecant(ode3,X,0,195,0,0,t0,t1,n+1,v0=45.0)
    plt.plot(X,Y,'-k',linewidth=2)
    
    plt.show()
    return X, Y
