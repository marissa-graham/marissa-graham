# Call this lab finite_difference.py
import numpy as np
from scipy import linalg as la
import matplotlib.pyplot as plt

def prob1(N,a,b):
    """
    Define u(x) = sin( (x + np.pi)**2 - 1).

    return x = np.linspace(a,b,N+1)[1:-1] 
    (the interior points on a grid with N equal subintervals),
    your values approximating .5*u''(x) - u'(x), 
    and the differentiation matrix M = .5*D2 - D1

    return x, approximation, M
    """
    hinv = N/(b-a)
    one = np.ones(N+1)
    oneless = np.ones(N)
    x = np.linspace(a,b,N+1)
    D2 = np.diag(-2*one)+np.diag(oneless,k=1)+np.diag(oneless,k=-1)
    D1 = np.diag(-1*oneless,k=-1)+np.diag(oneless,k=1)
    
    u = np.sin((x+np.pi)**2-1)
    u1, u2 = np.zeros_like(u), np.zeros_like(u)
    u1[0], u1[-1] = -u[0]*hinv*.5, u[-1]*hinv*.5
    u2[0], u2[-1] = u[0]*hinv**2, u[-1]*hinv**2
    
    approximation = 0.5*hinv**2*np.dot(D2,u)+0.5*u2 - 0.5*hinv*np.dot(D1,u)-u1
    return x[1:-1], approximation[1:-1], .5*D2-D1

def bvp(f, epsilon, alpha, beta, N):
    
    x = np.linspace(0,1,N+1)[1:-1]
    h = 1./N
    
    b = f(x)
    b[0] -= alpha*(epsilon+h/2.)/h**2
    b[-1] -= beta*(epsilon-h/2.)/h**2
    
    main = -2.*epsilon*np.ones(N-1)
    sub = (epsilon+h/2.)*np.ones(N-2)
    sup = (epsilon-h/2.)*np.ones(N-2)
    A = N**2*(np.diag(main)+np.diag(sub,-1)+np.diag(sup,1))
    
    soln = np.zeros(N+1)
    soln[0] = alpha
    soln[-1] = beta
    soln[1:-1] = la.solve(A,b)
    return np.linspace(0,1,N+1), soln

def prob2(N,epsilon):
    """ Numerically approximate the solution of the bvp
    epsilon*u''(x) - u'(x) = f(x), x in (0,1),
    u(0) = 1, u(1) = 3, 
    on the grid x = np.linspace(a,b,N+1).

    Plot your solution. 
    """
    def f(x):
        return -np.ones_like(x)
    x, U = bvp(f, epsilon, 1, 3, N)
    plt.plot(x,U)
    plt.show()
    return U

def prob3(epsilon):
    """ 
    Use a log-log plot to demonstrate 2nd order convergence of the finite
    difference method used in approximating the solution of the problem 2 in 
    the lab. 

    Do not return anything. 
    """
    num_approx = 10 # Number of Approximations
    N = 5*np.array([2**j for j in range(num_approx)])
    h, max_error = (1.-0)/N[:-1], np.ones(num_approx-1)
    # Best numerical solution, used to approximate the true solution.
    # bvp returns the grid, and the grid function, approximating the solution
    # with N subintervals of equal length.
    def f(x):
        return -np.ones_like(x)
    mesh_best, num_sol_best = bvp(f, epsilon=.1, alpha=1, beta=3, N=N[-1])
    for j in range(len(N)-1):
        mesh, num_sol = bvp(f, epsilon=.1, alpha=1, beta=3, N=N[j])
        #print num_sol.shape
        max_error[j] = np.max(np.abs(num_sol- num_sol_best[::2**(num_approx-j-1)]))
    plt.loglog(h,max_error,'.-r',label="$E(h)$")
    plt.loglog(h,h**(2.),'-k',label="$h^{\, 2}$")
    plt.xlabel("$h$")
    plt.legend(loc='best')
    plt.show()
    print "The order of the finite difference approximation is about ", ( (np.log(max_error[0]) - np.log(max_error[-1]) )/( np.log(h[0]) - np.log(h[-1]) ) ), "."

""" For problems 4-6, you will need to write (& use :) a function 
that solves a general second order linear bvp with Dirichlet 
conditions at both endpoints:

c1(x)u''(x) + c2(x)u'(x) + c3(x)u(x) = f(x), x in (a,b)
u(a) = alpha, u(b) = beta. 

An appropriate function signature would be something like 
the following: 

def fd_order2(N,a,b,alpha,beta,c1,c2,c3,f): 

    return np.linspace(a,b,N+1), numerical_approximation

"""
# Set up one function that'll be a solver
def bvp_order2(N,a,b,alpha,beta,a1,a2,a3,f):
    x = np.linspace(a,b,N+1)
    h = (b-a)/N

    c1 = a1(x)
    c2 = a2(x)
    c3 = a3(x)

    sub = (c1-0.5*h*c2)
    sub = sub[1:-2]
    diag = (-2.*c1 + h**2*c3)[1:-1]
    sup = (c1 + 0.5*h*c2)[2:-1]

    A = np.diag(sub,-1) + np.diag(diag) + np.diag(sup,1)

    F = np.zeros(N-1)
    for i in xrange(N-1):
        F[i] = f(x[i+1])
    F[0] -= alpha*(a1(0)-0.5*h*a2(0))/h**2
    F[-1] -= beta*(a1(x[-1])+0.5*h*a2(x[-1]))/h**2

    U = np.zeros(N+1)
    U[1:-1] = la.solve(1./h**2*A,F)
    # append alpha and beta to the beginning and end of U
    U[0] = alpha
    U[-1] = beta

    return U

def prob4(N=10):
    """ Numerically approximate the solution of the problem 4 in the lab.

    return x = np.linspace(a,b,N+1), and the approximation u at x

    You can check your answer with the following: when N = 10 and epsilon = .1,
    the approximation should be

    approximation = 
    [ 0.         -0.0626756  -0.07445629 -0.07518171 -0.07269296 -0.06765727
     -0.05673975 -0.02697816  0.06334385  0.32759416  1.        ]
    """
    f = lambda x:np.cos(x)
    c1 = lambda x:0.1*np.ones_like(x)
    c2 = lambda x:np.zeros_like(x)
    c3 = lambda x:-4.*(np.pi-x**2)
    return bvp_order2(N,0.,0.5*np.pi,0,1,c1,c2,c3,f)

def prob5(N=10):
    """ Numerically approximate the solution of the problem 5 in the lab.

    return x = np.linspace(a,b,N+1), and the approximation u at x

    You can check your answer with the following: when N = 10 and epsilon = .001,
    the approximation should be

    approximation = 
    [-2.         -0.93289014 -1.2830758   0.29230221 -0.13495904  1.13159506
      2.00336499  0.49595211  0.76087388 -0.66890253  0.        ]
    """
    pi = np.pi
    pi2 = np.pi**2
    solns = []
    for epsilon in [0.1,0.01,0.001]:
        f = lambda x: -epsilon*pi2*np.cos(pi*x) - pi*x*np.sin(pi*x)
        c1 = lambda x:epsilon*np.ones_like(x)
        c2 = lambda x:x
        c3 = lambda x:np.zeros_like(x)
        solns.append(bvp_order2(N,-1.,1.,-2.,0.,c1,c2,c3,f))

    return solns[0], solns[1], solns[2]


def prob6(N=10):
    """ Numerically approximate the solution of the problem 6 in the lab.

    return x = np.linspace(a,b,N+1), and the approximation u at x

    You can check your answer with the following: when N = 20 and epsilon = .05,
    the approximation should be 

    approximation = 
    [  0.95238095   1.16526611   1.45658263   1.868823     2.47619048
       3.41543514   4.95238095   7.61904762  12.38095238  19.80952381
      24.76190476  19.80952381  12.38095238   7.61904762   4.95238095
       3.41543514   2.47619048   1.868823     1.45658263   1.16526611
       0.95238095]


    """
    solns = []
    f = lambda x:np.zeros_like(x)
    c2 = lambda x:4.*x
    c3 = lambda x:2*np.ones_like(x)
    for eps in [0.05, 0.02]:
       c1 = lambda x:eps*np.ones_like(x) + x**2
       alpha = 1./(1+eps)
       beta = alpha
       solns.append(bvp_order2(N,-1.,1.,alpha,beta,c1,c2,c3,f))
    return solns[0],solns[1]



