
# coding: utf-8

# In[4]:

# Call this lab rivercrossing.py
import numpy as np
from scipy.misc import derivative as deriv
from scipy.integrate import quad
import matplotlib.pyplot as plt
from scipy.optimize import fsolve
from scipy.interpolate import BarycentricInterpolator



def prob1(y):
    """
    y is a curve (function) defined on [-1,1] that is passed in as an argument
    return T[y(x)]  (the time required )
    
    Assume that the current is given by c(x) = −.7(x − 1)(x + 1).
    (This function assumes, for example, that the current is faster near the center
    of the river.) Write a Python function that accepts as arguments a function y,
    its derivative y′, and an x-value, and returns L(x, y(x), y (x)) 
    Use that function to define a second function that
    numerically computes T[y] for a given path y(x).
    """
    def L(x):
        """Accepts an array y, approximates its derivative, and calculates
        L(x,y,y') at the given x value."""
        yprime = deriv(y,x)
        c = -0.7*(x-1)*(x+1)
        alpha = 1.0/np.sqrt(1-c**2)
        L = alpha*np.sqrt(1+(alpha*yprime)**2) - alpha**2*c*yprime
        return L
    return quad(L,-1,1)[0]


def prob2(y,yprime):
    """ 
    y is a curve (function) defined on [-1,1] that is passed in as an argument
    Find your upper and lower bounds on T[y].
    return the tuple (lower,upper)
    """
    
    Y = lambda x: 1/yprime(x)
    lower = quad(Y,-1,1)[0]
    l = lambda x: 2*x+2
    
    upper = prob1(l)
    
    return (lower, upper)
    
#y = lambda x:2.5*x+2.5
#yprime = lambda x:2.5

#print prob2(y, yprime)

# pull cheb(N) from a previous lab
def cheb(N):
    x =  np.cos((np.pi/N)*np.linspace(0,N,N+1))
    x.shape = (N+1,1)
    lin = np.linspace(0,N,N+1)
    lin.shape = (N+1,1)

    c = np.ones((N+1,1))
    c[0], c[-1] = 2., 2.
    c = c*(-1.)**lin
    X = x*np.ones(N+1) # broadcast along 2nd dimension (columns)

    dX = X - X.T

    D = (c*(1./c).T)/(dX + np.eye(N+1))
    D  = D - np.diag(np.sum(D.T,axis=0))
    x.shape = (N+1,)
    # Here we return the differentation matrix and the Chebychev points, 
    # numbered from x_0 = 1 to x_N = -1
    return D, x

def prob3():
    """ 
    Find and plot your solution.

    """
    c = lambda x: -.7*(x-1)*(x+1)
    a = lambda x: 1.0/np.sqrt(1-c(x)**2)
    N = 20
    D, x = cheb(N)
    
    def f(y):
        A = D.dot(a(x)**3*D.dot(y)/np.sqrt(1+(a(x)*D.dot(y))**2) - a(x)**2*c(x))
        A[0], A[-1] = y[0] - 5, y[-1]
        return A
    
    sol = fsolve(f, np.ones(N+1))
    num_sol = BarycentricInterpolator(x, sol)
    
    x = np.linspace(-1,1,400)
    y =  num_sol.__call__(x)
    
    plt.plot(x,y,label='approx')
    plt.show()

#prob3()

def prob4():
    """ 
    Produce the plot asked for in the lab. 

    """
    c = lambda x: -.7*(x-1)*(x+1)
    a = lambda x: 1.0/np.sqrt(1-c(x)**2)
    N = 20
    D, x = cheb(N)
    
    def f(y):
        A = D.dot(a(x)**3*D.dot(y)/np.sqrt(1+(a(x)*D.dot(y))**2) - a(x)**2*c(x))
        A[0], A[-1] = y[0] - 5, y[-1]
        return A
    
    sol = fsolve(f, np.ones(N+1))
    num_sol = BarycentricInterpolator(x, sol)
    num_diff = BarycentricInterpolator(x, D.dot(sol))
    
    x = np.linspace(-1,1,400)
    y = num_sol.__call__(x)
    yp = num_diff.__call__(x)
    
    theta = np.arcsin((-2*c(x) + np.sqrt(4*c(x)**2-4*(c(x)**2-yp**2)*(1+yp**2)))/(2*(1+yp**2)))
    
    plt.plot(x,theta)
    plt.ylim(0,np.pi/2.0)
    plt.show()
    
#prob4()


# In[ ]:



