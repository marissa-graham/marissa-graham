import numpy as np
from matplotlib import pyplot as plt
from math import floor

def upwind(u0, a, xmin, xmax, t_final, nt):
    """ Solve the advection equation with periodic
    boundary conditions on the interval [xmin, xmax]
    using the upwind finite volume scheme.
    Use u0 as the initial conditions.
    a is the constant from the PDE.
    Use the size of u0 as the number of nodes in
    the spatial dimension.
    Let nt be the number of spaces in the time dimension
    (this is the same as the number of steps if you do
    not include the initial state).
    Plot and show the computed solution along
    with the exact solution. """
    dt = float(t_final) / nt
    # Since we are doing periodic boundary conditions,
    # we need to divide by u0.size instead of (u0.size - 1).
    dx = float(xmax - xmin) / u0.size
    lambda_ = a * dt / dx
    u = u0.copy()
    for j in xrange(nt):
        # The Upwind method. The np.roll function helps us
        # account for the periodic boundary conditions.
        u -= lambda_ * (u - np.roll(u, 1))
    # Get the x values for the plots.
    x = np.linspace(xmin, xmax, u0.size+1)[:-1]
    # Plot the computed solution.
    plt.plot(x, u, label='Upwind Method')
    # Find the exact solution and plot it.
    distance = a * t_final
    roll = int((distance - floor(distance)) * u0.size)
    plt.plot(x, np.roll(u0, roll), label='Exact solution')
    # Show the plot with the legend.
    plt.legend(loc='best')
    plt.show()

def lax_wendroff(u0, a, xmin, xmax, t_final, nt):
    """ Solve the advection equation with periodic
    boundary conditions on the interval [xmin, xmax]
    using the Lax-Wendroff finite volume scheme.
    Use u0 as the initial conditions.
    a is the constant from the PDE.
    Use the size of u0 as the number of nodes in
    the spatial dimension.
    Let nt be the number of spaces in the time dimension
    (this is the same as the number of steps if you do
    not include the initial state).
    Plot and show the computed solution along
    with the exact solution.
    Hint: equations 1.9 and 1.3"""
    dt = float(t_final) / nt
    # Since we are doing periodic boundary conditions,
    # we need to divide by u0.size instead of (u0.size - 1).
    dx = float(xmax - xmin) / u0.size
    lambd = a * dt / dx
    u = u0.copy()
    u1 = u0.copy()
    for j in xrange(nt):
        # The Upwind method. The np.roll function helps us
        # account for the periodic boundary conditions.
        # CHANGE THIS LINE
        u -= lambd * (u - np.roll(u,1))
        #u -= tdel/xdel(F_i+1/2 - F_i-1/2)
        Uplus = np.roll(u1,-1)
        Uminus = np.roll(u1,1)
        u1 -= lambd*(u1 - Uminus + 0.5/dx*(dx-a*dt)*(Uplus-2*u1+Uminus))
     
    # Get the x values for the plots.
    x = np.linspace(xmin, xmax, u0.size+1)[:-1]
    # Plot the computed solution.
    #plt.plot(x, u, label='Upwind Method')
    plt.plot(x,u1, label='Lax-Wendroff Method')
    # Find the exact solution and plot it.
    distance = a * t_final
    roll = int((distance - floor(distance)) * u0.size)
    plt.plot(x, np.roll(u0, roll), label='Exact solution')
    # Show the plot with the legend.
    plt.legend(loc='best')
    plt.show()

def minmod(a,b):
    for j in xrange(len(a)):
        if a[j]*b[j] > 0:
            if np.abs(a[j]) <= np.abs(b[j]):
                pass
            else:
                a[j] = b[j]
        else:
            a[j] = 0
    return a

def minimod(u0, a, xmin, xmax, t_final, nt):
    """ Solve the advection equation with periodic
    boundary conditions on the interval [xmin, xmax]
    using the minimod finite volume scheme.
    Use u0 as the initial conditions.
    a is the constant from the PDE.
    Use the size of u0 as the number of nodes in
    the spatial dimension.
    Let nt be the number of spaces in the time dimension
    (this is the same as the number of steps if you do
    not include the initial state).
    Plot and show the computed solution along
    with the exact solution. """
    dt = float(t_final) / nt
    # Since we are doing periodic boundary conditions,
    # we need to divide by u0.size instead of (u0.size - 1).
    dx = float(xmax - xmin) / u0.size
    lambd = a * dt / dx
    u = u0.copy()
    u1 = u0.copy()
    u2 = u0.copy()
    for j in xrange(nt):
        # The Upwind method. The np.roll function helps us
        # account for the periodic boundary conditions.
        # CHANGE THIS LINE
        u -= lambd * (u - np.roll(u,1))
        #u -= tdel/xdel(F_i+1/2 - F_i-1/2)
        Uplus = np.roll(u1,-1)
        Uminus = np.roll(u1,1)
        u1 -= lambd*(u1 - Uminus + 0.5/dx*(dx-a*dt)*(Uplus-2*u1+Uminus))
        
        m1 = minmod(np.roll(u2,-1)/dx, u2/dx)
        m2 = minmod(u2/dx, np.roll(u2,1)/dx)
        u2 -= lambd*(u2 - np.roll(u2,1) + 0.5*(dx-a*dt)*(m1 - m2))
     
    # Get the x values for the plots.
    x = np.linspace(xmin, xmax, u0.size+1)[:-1]
    # Plot the computed solution.
    #plt.plot(x, u, label='Upwind Method')
    #plt.plot(x,u1, label='Lax-Wendroff Method')
    plt.plot(x,np.roll(u2,int(-nx/30)), label='Minmod Method')
    # Find the exact solution and plot it.
    distance = a * t_final
    roll = int((distance - floor(distance)) * u0.size)
    plt.plot(x, np.roll(u0, roll), label='Exact solution')
    # Show the plot with the legend.
    plt.legend(loc='best')
    plt.show()

def test():
    for nx in [30,60,120,240]:
        nt = nx * 3 // 2
        x = np.linspace(0., 1., nx+1)[:-1]
        u0 = np.exp(-(x - .3)**2 / .005)
        arr = (.6 < x) & (x < .7 )
        u0[arr] += 1.
        # Run the simulation.
        minimod(u0, 1.2, 0, 1, 1.2, nt)