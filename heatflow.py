
# coding: utf-8

# In[2]:

# Call your solutions file heatflow.py
import numpy as np
from matplotlib import animation
from matplotlib import pyplot as plt
from scipy import linalg as la

def prob1():
    eta = 0.05
    h = 1/6.
    k = .4/10.
    l = eta/h**2*k
    
    def u(x):
        # Return u(x,0)
        return 2*np.maximum(.2-np.abs(x-.5),np.zeros(x.shape[0]))
    
    # approximate at t=.4 using 6 subintervals in x and 10 subintervals in t
    domain = np.linspace(0,1,7)
    U = u(domain)
    U[0], U[-1] = 0, 0
    plt.plot(domain, U)
    #plt.show()
    
    
    for i in xrange(1,11):
        temp = U
        U[1:] += l*temp[:-1]
        U[:-1] += l*temp[1:]
        U -= 2*l*temp
        
    plt.plot(domain, U)
    plt.show()


def prob2():
    for n in [70,66]:
        eta = 1
        h = (24.)/140.
        k = 1./n
        l = eta*k/h**2
        def u(x):
            return np.maximum(1.-x**2, np.zeros(x.shape[0]))

        domain = np.linspace(-12,12,141)
        U = u(domain)
        U[0], U[-1] = 0, 0
        bigU = np.zeros((n+1,U.shape[0]))
        bigU[0,:] = U

        for j in xrange(1,n+1):
            bigU[j,1:-1] = bigU[j-1,1:-1] + l*(bigU[j-1,2:]-2.*bigU[j-1,1:-1] + bigU[j-1,:-2])
            bigU[j,0] = 0.
            bigU[j,-1] = 0.
        f = plt.figure()
        plt.axes(xlim=(-12,12),ylim=(0,1))
        line, = plt.plot([],[])

        def animate(i):
            line.set_data(domain, bigU[i,:])
            return line,

        a = animation.FuncAnimation(f, animate, frames=n+1, interval=50)
        plt.show()
    
    
def prob3():
    # Reproduce the loglog plot in the lab to demonstrate 2nd order convergence of the 
    # Crank-Nicolson method. 
            
    eta = 1.0
    def u(x):
        return np.maximum(1.-x**2, np.zeros(x.shape[0]))
    
    U = np.zeros((641,6),dtype=np.float)
    errors = np.zeros(6)
    nvals = np.array([20,40,80,160,320,640])
    
    # Find solutions
    for i in xrange(6):
        n = nvals[i]
        
        k = 1.0/n
        h = 24./n
        l = eta*k/(2*h**2)
        
        diag = np.ones(n-1)
        offdiag = np.ones(n-2)
        B = np.diag((1+2*l)*diag)+np.diag(-l*offdiag,-1)+np.diag(-l*offdiag,1)
        A = np.diag((1-2*l)*diag)+np.diag(l*offdiag,-1)+np.diag(l*offdiag,1)
        
        soln = u(np.linspace(-12,12,n+1))[1:-1]
        soln[0], soln[-1] = 0., 0.
        U[::640/n,i][1:-1] = soln
        
        for j in xrange(1,n+1):
            soln = la.solve(B,np.dot(A,U[::640/n,i][1:-1]))
            #print "(i,j) = (", i, ",", j, "), " , np.argmax(np.dot(A,U[::640/n,i][1:-1])), "," , np.argmax(soln)
            U[::640/n,i][1:-1] = soln
        
      
    # Calculate errors
    for i in xrange(6):
        n = nvals[i]
        errors[i] = la.norm(U[::640/n,i]-U[::640/n,-1],ord=np.inf)
    
    plt.loglog(1./nvals[:-1],(1./nvals**2)[:-1])
    plt.loglog(1./nvals[:-1],errors[:-1])
    plt.show()

