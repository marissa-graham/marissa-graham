# Call this lab finite_element.py

import numpy as np
from scipy import linalg as la
from scipy import sparse as sp
from scipy.sparse import linalg as sparla
import matplotlib.pyplot as plt

def solve(x, a, b, e):
    # Solver for problems of the form ey'' - y' = -1, y(0) = a, y(1) = b
    # accepts e, a, b, and a grid to solve on
    h = np.diff(x)
    print h
    n = len(h)
    
    # Build the matrix A and the right-hand vector l
    A = np.zeros((n+1,n+1))
    for i in xrange(1,n):
        # a(i,j) = e/h[i+1] + 0.5  if j=i+1
        #         -e/h[i]-e/h[i+1] if j=i
        #          e/h[i] - 0.5    if j=i-1
        
        # i goes like 1,2,3,4
        # subdiag (j = i+1)
        A[i,i-1] = e/h[i-1] + 0.5
        # have an i,i for main diag (j = i)
        A[i,i] = -e/h[i-1]-e/h[i]
        # superdiag (j = i-1)
        A[i,i+1] = e/h[i] - 0.5
        
        print A[i,i-1], A[i,i], A[i,i+1]
         
    
    A[0,0] = 1
    A[-1,-1] = 1
    
    
    A = sp.csc_matrix(A)
    
    phi = np.zeros(n+1)
    phi[0] = a
    for i in xrange(1,n):
        phi[i] = -0.5*(h[i-1]+h[i])
    phi[-1] = b
    
    
    # Sparse linear solver
    k = sparla.spsolve(A,phi)
    
    return k

def y(x, a, b, e):
    return a + x + (b-a-1)*(np.exp(x/e) - 1)/(np.exp(1./e)-1)

def prob1():
    """
    Plot the true solution, along with your numerical solution.
    """
    #d = np.array([0,0.1,0.2,0.5,0.9,1])
    d = np.linspace(0,1,101)
    x = solve(d,2.0,4.0,0.02)
    plt.plot(d,x)
    dom = np.linspace(0,1,101)
    plt.plot(dom,y(dom,2.0,4.0,0.02))
    plt.show()

def prob2():
    """ 
    Plot the true solution, and your numerical solutions. Reproduce the 
    graph shown in the lab.  

    """
    d = np.linspace(0,1,15)
    even_grid = d
    clustered_grid = d**(1./8)
    sol1 = solve(even_grid, 2.0, 4.0, 0.02)
    sol2 = solve(clustered_grid, 2.0, 4.0, 0.02)
    plt.plot(d, sol1)
    plt.plot(clustered_grid, sol2,marker='.')
    plt.plot(np.linspace(0,1,100), y(np.linspace(0,1,100),2.0,4.0,0.02))
    plt.show()

def prob3():
    """ 
    Plot and show a log-log graph, showing both the error in the 
    second order finite element approximations, and the error in the 
    pseudospectral approximations. 

    You can verify the error for the second order finite element 
    approximations using the graph shown in the lab. 

    """
    pass

