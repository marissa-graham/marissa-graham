from __future__ import division
import numpy as np
from matplotlib import pyplot as plt

def initialize_all(y0, t0, t, n):
    """ An initialization routine for the different ODE solving
    methods in the lab. This initializes Y, T, and h. """
    if isinstance(y0, np.ndarray):
        Y = np.empty((n, y0.size)).squeeze()
    else:
        Y = np.empty(n)
    Y[0] = y0
    T = np.linspace(t0, t, n)
    h = float(t - t0) / (n - 1)
    return Y, T, h

def euler(f, y0, t0, t, n):
    """ Use the Euler method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept two arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    Y, T, h = initialize_all(y0, t0, t, n)
    for i in xrange(n-1):
        Y[i+1] = Y[i] + h*f(T[i],Y[i])
    return Y
    

def midpoint(f, y0, t0, t, n):
    """ Use the midpoint method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept two arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    Y, T, h = initialize_all(y0, t0, t, n)
    for i in xrange(n-1):
        Y[i+1] = Y[i] + h*f(T[i] + h/2, Y[i] + h/2*f(T[i],Y[i]))
    return Y

def RK4(f, y0, t0, t, n):
    """ Use the RK4 method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept two arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    Y, T, h = initialize_all(y0, t0, t, n)
    for i in xrange(n-1):
        k1 = f(T[i] ,Y[i])
        k2 = f(T[i] + h/2, Y[i] + h*k1/2)
        k3 = f(T[i] + h/2, Y[i] + h*k2/2)
        k4 = f(T[i+1], Y[i] + h*k3)
        Y[i+1] = Y[i] + h/6*(k1 + 2*k2 + 2*k3 + k4)
    return Y

def prob1():
    """ Test the accuracy of the Euler method using the
    initial value problem y' + y = 2 - 2x, 0 <= x <= 2, y(0) = 0.
    Plot your solutions over the given domain with n as 11, 21, and 41.
    Also plot the exact solution. 
    Show the plot. """
    
    #f = lambda x,y:2 - 2*x - y
    f = lambda x, y:-2*x + 4 + y
    y0 = 0
    t0 = 0
    t = 2
    
    for n in [11,21,41]:
        Y = euler(f, y0, t0, t, n)
        plt.plot(np.linspace(t0, t, n), Y)
    
    #y = lambda x:4 - 2*x - 4*np.exp(-x)
    y = lambda x:-2 + 2*x + 2*np.exp(x)
    dom = np.linspace(t0, t, 41)
    plt.plot(dom, y(dom))
    plt.show()  

def prob2():
    #h = np.array([.0125,.025,.05,.1,.2])
    h = np.array([.2,.1,.05,.025,.0125])
    n_values = 2/h + 1
    n_values = n_values.astype(int)
    
    f = lambda x,y:y - 2*x + 4
    t0 = 0
    t = 2
    y0 = 0
    y2 = -2 + 2*2 + 2*np.exp(2)
    
    eulervals = np.empty(5)
    midpointvals = np.empty(5)
    RK4vals = np.empty(5)
    
    i = 0
    for n in n_values:
        yapprox = euler(f, y0, t0, t, n)[-1]
        eulervals[i] = np.abs(yapprox - y2)/np.abs(y2)
        
        yapprox = midpoint(f, y0, t0, t, n)[-1]
        midpointvals[i] = np.abs(yapprox - y2)/np.abs(y2)
        
        yapprox = RK4(f, y0, t0, t, n)[-1]
        RK4vals[i] = np.abs(yapprox - y2)/np.abs(y2)
        
        i += 1
        
    plt.loglog(h, eulervals)
    plt.loglog(h, midpointvals)
    plt.loglog(h, RK4vals)
    plt.show()  
    
def prob3(n):
    """ Use the RK4 method to solve for the simple harmonic oscillator
    problem described in the problem about simple harmonic oscillators.
    Return the array of values at the equispaced points. """
    
    k, m = 1, 1
    f = lambda t, x: np.array([x[1], -k/m*x[0]])
    t0 = 0
    t = 20
    y0 = np.array([2,-1])
    Y, T, h = initialize_all(y0, t0, t, n)
    yapprox = RK4(f, y0, t0, t, n)
    plt.plot(T, yapprox[:,0])
    
    k, m = 1, 3
    yapprox = RK4(f, y0, t0, t, n)
    plt.plot(T, yapprox[:,0])
    
    plt.show()

def prob4(n, gamma):
    """ Use the RK4 method to solve for the damped harmonic oscillator
    problem described in the problem about damped harmonic oscillators.
    Return the array of values at the equispaced points. """
    f = lambda t, x: np.array([x[1], -gamma*x[1] - x[0]])
    t0 = 0
    t = 20
    y0 = np.array([1, -1])
    
    Y, T, h = initialize_all(y0, t0, t, n)
    yapprox = RK4(f, y0, t0, t, n)
    
    #print yapprox[:,0].shape
    return yapprox[:,0]
    #plt.plot(T, yapprox[:,0])
    #plt.show()
    
    #print np.abs(yapprox[-1] - y20)/np.abs(y20)
    
def prob5(n, gamma, omega):
    """ Use the RK4 method to solve for the damped and forced harmonic 
    oscillator problem. Return the array of values at the n equally spaced points. 
    Plot and show your solution.
    """
    f = lambda x, y: np.array([y[1], np.cos(omega*x) - gamma/2*y[1] - y[0]])
    t0 = 0
    t = 40
    y0 = np.array([2, -1])
    
    Y, T, h = initialize_all(y0, t0, t, n)
    #print RK4(f, y0, t0, t, n)[:,0].shape
    return RK4(f, y0, t0, t, n)[:,0]
    #plt.plot(T, yapprox[:,0])
    #plt.show()

#prob2()
#prob4(10,1.)
#prob5(10,0.5,1.5)









# optional
def backwards_euler(f, fsolve, y0, t0, t, n):
    """ Use the backward Euler method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept two arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    fsolve is a function that solves the equation
    y(x_{i+1}) = y(x_i) + h f(x_{i+1}, y(x_{i+1}))
    for the appropriate value for y(x_{i+1}).
    It should accept three arguments.
    The first should be the value of y(x_i).
    The second should be the distance between values of t.
    The third should be the value of x_{i+1}.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    pass

# Optional
def modified_euler(f, y0, t0, t, n):
    """ Use the modified Euler method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept two arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    pass

# I'm giving you this one.
# Please run it so you can see the error plots.
def compare_accuracies(n):
    """ Test the accuracies of the Euler, backwards Euler, modified Euler,
    midpoint, and RK4 methods using initial value problem
    y' + y = 2 - 2x, 0 <= x <= 2, y(0) = 0.
    Use n equispaced points from 0 to 2.
    Make a plot of the absolute error of each approximation.
    Show it at the end. """
    # The derivative function corresponding to the ODE
    f = lambda x, y: 2 - y - 2 * x
    # Solving routine for the backwards-euler method.
    # fsolve = lambda yi, h, xi1: (yi + 2 * h * (1 - xi1)) / (1 + h)
    # Values for the horizontal axis in the plot.
    T = np.linspace(0, 2, n)
    # Initialize exact solution for comparison.
    exact = 4 - 2 * T - 4 * np.exp(-T)
    # Plot all the errors.
    plt.plot(T, np.absolute(euler(f, 0, 0, 2, n) - exact))
    # Optional.
    # plt.plot(T, np.absolute(backwards_euler(fsolve, 0, 0, 2, n) - exact))
    plt.plot(T, np.absolute(euler(f, 0, 0, 2, n) - exact))
    # The next method is optional.
    # plt.plot(T, np.absolute(modified_euler(f, 0, 0, 2, n) - exact))
    plt.plot(T, np.absolute(midpoint(f, 0, 0, 2, n) - exact))
    plt.plot(T, np.absolute(RK4(f, 0, 0, 2, n) - exact))
    # Tweak the lower bound so it shows that some of them
    # have nearly zero error.
    mn, mx = plt.ylim()
    mn -= .1 * mx
    plt.ylim((mn, mx))
    # Show it.
    plt.show()