
# coding: utf-8

# In[7]:




# In[88]:

from __future__ import division
import numpy as np
from scipy import optimize
from scipy import linalg as la
from scipy.integrate import odeint
from matplotlib import pyplot as plt

##### Problem 1 #####

def linearized_init(M, m, l, q1, q2, q3, q4, r):
    '''
    Parameters:
    ----------
    M, m: floats
        masses of the rickshaw and the present
    l : float
        length of the rod
    q1, q2, q3, q4, r : floats
        relative weights of the position and velocity of the rickshaw, the
        angular displacement theta and the change in theta, and the control
    Return
    -------
    A : ndarray of shape (4,4)
    B : ndarray of shape (4,1)
    Q : ndarray of shape (4,4)
    R : ndarray of shape (1,1)
    '''
    
    g = 9.8 #m/s^2
    A = np.array([[0,1,0,0],[0,0,m*g/M,0],[0,0,0,1],[0,0,(g/(M*l))*(M+m),0]])
    #B = np.array([[0],[1/M],[0],[1/(M*l)]])
    B = np.array([0,1./M,0,1/(M*l)]).reshape(4,1)
    print B.shape
    Q = np.diag([q1,q2,q3,q4])
    R = np.array([[r]])
    
    return A,B,Q,R

##### Problem 2 #####

def find_P(A,B,Q,R):
    '''
    Parameters:
    ----------
    A, B, Q : ndarrays of shape (4,4)
    R : ndarray of shape (1,1)
    Returns
    -------
    P : the matrix solution of the Riccati equation
    '''
    
    # Create ARE to find root of (1.10)
    def ARE(P):
        
        # P is a vector with 16 entries, so have to reshape to matrix multiply
        P = np.reshape(P,(4,4))
        M = P.dot(A) + (A.T).dot(P) + Q - P.dot(B.dot(la.inv(R).dot(B.T.dot(P))))
        
        # Reshape to get 16 equations to pass into scipy.optimize
        return np.reshape(M,16)
    
    Z = np.random.randn(16)
    sol = optimize.root(ARE,Z)
    P = np.reshape(sol.x,(4,4))
    
    return P

def prob2():
    
    # Solve for P explicitly with created functions and given values
    A,B,Q,R = linearized_init(23, 5, 4, 1, 1, 1, 1, 5)
    P = find_P(A,B,Q,R)
    
    # Compute and return the eigenvalues of A - B(R^-1)(B^T)P
    M = A - B.dot(la.inv(R).dot(B.T.dot(P)))
    eigs = la.eig(M)[0]
    
    # Used given method la.solve_continuous.are for comparison
    P2 = la.solve_continuous_are(A,B,Q,R)
    M2 = A - B.dot(la.inv(R).dot(B.T.dot(P2)))
    eigs2 = la.eig(M2)[0]
    
    print "One eigenvalue is positive (two others have positive real part), so for this P, we might not have z'->0."
    print "The la.solve_continuous_are method gave a P that made all real parts of eigenvalues negative, as desired."
    return eigs, eigs2

##### Problem 3 #####

def rickshaw(tv, X0, A, B, Q, R_inv, P, plot=True):
    '''
    Parameters:
    ----------
    tv : ndarray of time values, with shape (n+1,)
    X0 :
    A, B, Q : ndarrays of shape (4,4)
    R_inv : ndarray of shape (1,1), inverse of R
    P : ndarray of shape (4,4)
    Returns
    -------
    Z : ndarray of shape (n+1,4), the state vector at each time
    U : ndarray of shape (n,), the control values
    '''
    
    
    # Set up the ODE z' = Mz
    M = A - B.dot(R_inv).dot(B.T).dot(P)
    
    def f(y,t):
        return np.dot(M,y)
    
    Z = odeint(f,X0,tv)
    
    # Now solve u = -R_invB^TPz
    U = -R_inv.dot(B.T).dot(P).dot(Z.T).flatten()
    
    plt.plot(tv, Z[:,0], label='$x$')
    plt.plot(tv, Z[:,1], label="$x'$")
    plt.plot(tv, Z[:,2], label="$\Theta$")
    plt.plot(tv, Z[:,3], label="$\Theta'$")
    plt.plot(tv, U, label="$u$")
    plt.title('P is found using root')
    plt.legend(loc='best')
    plt.show()
    
    return Z,U

##### Problem 4 #####

def prob4():
    
    M, m = 23., 5.
    l = 4.
    q1, q2, q3, q4 = 1., 1., 1., 1.
    r = 10.
    tf = 15
    X0 = np.array([-1, -1, .1, -.2])
    n = 400
    t = np.linspace(0, tf, n)
    A, B, Q, R = linearized_init(M, m, l, q1, q2, q3, q4, r)
    P1 = find_P(A, B, Q, R)   
    X1, u1 = rickshaw(t, X0, A, B, Q, la.inv(R), P1, plot=True)
    
        
    
    P2 = la.solve_continuous_are(A, B, Q, R)   
    X2, u2 = rickshaw(t, X0, A, B, Q, la.inv(R), P2)
    
    tf = 60
    t = np.linspace(0,tf,n)
    plt.plot(t, X2[:,0], label='$x$')
    plt.plot(t, X2[:,1], label="$x'$")
    plt.plot(t, X2[:,2], label="$\Theta$")
    plt.plot(t, X2[:,3], label="$\Theta'$")
    plt.plot(t, u2, label="$u$")
    plt.title('P is found using solve_continuous_are')
    plt.legend(loc='best')
    plt.show()


def prob5():
    M, m = 23., 5.
    l = 4.
    q1, q2, q3, q4 = 1., 1., 1., 1.
    r = 10.
    tf = 60
    #X0 = np.array([-1, -1, .1, -.2])
    n = 400
    t = np.linspace(0, tf, n)
    A, B, Q, R = linearized_init(M, m, l, q1, q2, q3, q4, r)
    P1 = find_P(A, B, Q, R)   
    for i in [np.array([-1, -1, .1, -.2]), np.array([-1,-1,.15, -.25]), np.array([-1, -1, .5, .3]), np.array([-1, -1, -.4, -.6]), np.array([-1, -1, .06, -.02])]:
        P2 = la.solve_continuous_are(A, B, Q, R)   
        print 'X0 = ' + str(i)
        X2, u2 = rickshaw(t, i, A, B, Q, la.inv(R), P2, plot=True)
    
    print "From the above plots, we note that u possesses a very steep (and physically unreasonable) beginning for initial conditions even within a fraction of np.array([-1, -1, .1, -.2])"
    print "This suggests that the control is very sensitive to initial conditions, with the lab chosen X0 being, in truth, the most reasonable"

prob2()
prob4()
prob5()


# In[ ]:



