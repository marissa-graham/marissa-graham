import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import newton

def EmbeddingAlg(param_list, guess, F):
    X = []
    for param in param_list:
        try:
            # Solve for x_value making F(x_value, param) = 0.
            x_value = newton(F, guess, fprime=None, args=(param,), tol=1E-7, maxiter=50)
            # Record the solution and update guess for the next iteration.
            X.append(x_value)
            guess = x_value
        except RuntimeError:
            # If Newton's method fails, return a truncated list of parameters
            # with the corresponding x values.
            return param_list[:len(X)], X
        # Return the list of parameters and the corresponding x values.
    return param_list, X

def run_test():
    def F(x, lmbda):
        return x**2 + lmbda

    # Top curve shown in the bifurcation diagram
    C1, X1 = EmbeddingAlg(np.linspace(-5, 0, 200), np.sqrt(5), F)
    # The bottom curve
    C2, X2 = EmbeddingAlg(np.linspace(-5, 0, 200), -np.sqrt(5), F)
    plt.plot(C1,X1,C2,X2)
    plt.show()

def prob1():
    # Using the natural embedding algorithm, produce the plot 
    # requested in exercise 1 in the lab.
    def F(x, lmbda):
        return lmbda*x - x**3

    C1, X1 = EmbeddingAlg(np.linspace(5, -5, 200), np.sqrt(5), F)
    C2, X2 = EmbeddingAlg(np.linspace(5, -5, 200), -np.sqrt(5), F)
    C3, X3 = EmbeddingAlg(np.linspace(5, -5, 200), 0, F)
    plt.plot(C1,X1,C2,X2,C3,X3)
    plt.show()

def prob2():
    # Using the natural embedding algorithm, produce the plots 
    # requested in exercise 2 in the lab.
    
    def F(x, lmbda):
        return eta + lmbda*x - x**3
    
    eta = -1.
    C1, X1 = EmbeddingAlg(np.linspace(5, -5, 200), np.sqrt(5), F)
    C2, X2 = EmbeddingAlg(np.linspace(5, -5, 200), -np.sqrt(5), F)
    C3, X3 = EmbeddingAlg(np.linspace(5, -5, 200), 0, F)
    plt.plot(C1,X1,C2,X2,C3,X3)
    plt.show()
    
    eta = -.2
    C1, X1 = EmbeddingAlg(np.linspace(5, -5, 200), np.sqrt(5), F)
    C2, X2 = EmbeddingAlg(np.linspace(5, -5, 200), -np.sqrt(5), F)
    C3, X3 = EmbeddingAlg(np.linspace(5, -5, 200), 0, F)
    plt.plot(C1,X1,C2,X2,C3,X3)
    plt.show()
    
    eta = .2
    C1, X1 = EmbeddingAlg(np.linspace(5, -5, 200), np.sqrt(5), F)
    C2, X2 = EmbeddingAlg(np.linspace(5, -5, 200), -np.sqrt(5), F)
    C3, X3 = EmbeddingAlg(np.linspace(5, -5, 200), 0, F)
    plt.plot(C1,X1,C2,X2,C3,X3)
    plt.show()
    
    eta = 1.
    C1, X1 = EmbeddingAlg(np.linspace(5, -5, 200), np.sqrt(5), F)
    C2, X2 = EmbeddingAlg(np.linspace(5, -5, 200), -np.sqrt(5), F)
    C3, X3 = EmbeddingAlg(np.linspace(5, -5, 200), 0, F)
    plt.plot(C1,X1,C2,X2,C3,X3)
    plt.show()
    


def prob3():
    # Using the natural embedding algorithm, produce the plot 
    # requested in exercise 3 in the lab.
    r = 0.56
    
    def F(x, k):
        return r*x*(1-x/k) - x**2/(1+x**2)
    
    C1, X1 = EmbeddingAlg(np.linspace(2, 12, 200), 0.5, F)
    C2, X2 = EmbeddingAlg(np.linspace(12, 6.405, 200), 9.8, F)
    C3, X3 = EmbeddingAlg(np.linspace(8, 5.5, 200), 1.6, F)
    C4, X4 = EmbeddingAlg(np.linspace(8, 10.5, 200), 1.6, F)
    plt.plot(C1,X1,C2,X2,C3,X3, C4, X4)
    plt.show()