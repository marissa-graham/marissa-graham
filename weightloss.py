# Name it weightloss.py
import numpy as np
from scipy.integrate import ode
from scipy.integrate import odeint
from math import log
from matplotlib import pyplot as plt

# Fixed constants
rho_F = 9400.
rho_L = 1800.
gamma_F = 3.2
gamma_L = 22.
eta_F = 180.
eta_L = 230.
C = 10.4 # Forbes constant
beta_AT = 0.14 # Adaptive Thermogenesis
beta_TEF = 0.1 # Thermic Effect of Feeding
K=0

def forbes(F):
    C1 = C * rho_L / rho_F
    return C1 / (C1 + F)

def energy_balance(F, L, EI, PAL):
    p = forbes(F)
    a1 = (1. / PAL - beta_AT) * EI - K - gamma_F * F - gamma_L * L
    a2 = (1 - p) * eta_F / rho_F + p * eta_L / rho_L + 1. / PAL
    return a1 / a2

def weight_odesystem(t, y, EI, PAL):
    F, L = y[0], y[1]
    p, EB = forbes(F), energy_balance(F, L, EI, PAL)
    return np.array([(1 - p) * EB / rho_F , p * EB / rho_L])

def fat_mass(BW, age, H, sex):
    BMI = BW / H**2.
    if sex == 'male':
        return BW * (-103.91 + 37.31 * log(BMI) + 0.14 * age) / 100
    else:
        return BW * (-102.01 + 39.96 * log(BMI) + 0.14 * age) / 100

def prob1():
    """ Using the RK4 method, approximate the solution curve for a 
    single-stage weightloss intervention. 
    Plot and show your solution. 
    """
    BW = 160/2.20462
    age = 38
    H = 68/39.3701
    sex = "female"
    F0 = fat_mass(BW, age, H, sex)
    L0 = BW - F0
    print F0, L0
    EI = 2025
    PAL = 1.5
    t0 = 0
    
    f = lambda t, y: weight_odesystem(t, y, EI, PAL)
    
    r = ode(f).set_integrator('dopri5')
    r.set_initial_value(np.array([F0, L0]), t0)
    t1 = 1825
    dt = 1
    domain = np.arange(t1)
    fatmass = np.empty(t1)
    leanmass = np.empty(t1)
    i = 0
    while r.successful() and r.t < t1:
        #print r.integrate(r.t+dt)
        vals = r.integrate(r.t+dt)
        fatmass[i] = vals[0]
        leanmass[i] = vals[1]
        i += 1
    total = fatmass + leanmass
    kg = 1
    
    plt.plot(domain, kg*fatmass, domain, kg*leanmass, domain, kg*total)
    plt.show()
    

def prob2():
    """ Using the RK4 method, approximate the solution curve for a 
    two-stage weightloss intervention. 
    Plot and show your solution. 
    """
    BW = 160/2.20462
    age = 38
    H = 68/39.3701
    sex = "female"
    F0 = fat_mass(BW, age, H, sex)
    L0 = BW - F0
    print F0, L0
    EI = 1600
    PAL = 1.7
    t0 = 0
    
    f = lambda t, y: weight_odesystem(t, y, EI, PAL)
    
    r = ode(f).set_integrator('dopri5')
    r.set_initial_value(np.array([F0, L0]), t0)
    t1 = 16*7
    dt = 1
    domain = np.arange(t1*2)
    fatmass = np.empty(t1*2)
    leanmass = np.empty(t1*2)
    i = 0
    
    # Stage 1
    while r.successful() and r.t < t1:
        #print r.integrate(r.t+dt)
        vals = r.integrate(r.t+dt)
        fatmass[i] = vals[0]
        leanmass[i] = vals[1]
        i += 1
    
    # Stage 2
    F0 = fatmass[i-1]
    L0 = leanmass[i-1]
    BW = F0 + L0
    EI = 2025
    PAL = 1.5
    f = lambda t, y: weight_odesystem(t, y, EI, PAL)
    
    r = ode(f).set_integrator('dopri5')
    r.set_initial_value(np.array([F0, L0]), t0)
    while r.successful() and r.t < t1:
        vals = r.integrate(r.t+dt)
        fatmass[i] = vals[0]
        leanmass[i] = vals[1]
        i += 1
        
    total = fatmass + leanmass
    kg = 1
    
    plt.plot(domain, kg*fatmass, domain, kg*leanmass, domain, kg*total)
    plt.show()

    
def prob3():
    """ Using the RK4 method, approximate the predator-prey trajectories.
    Plot and show the solutions. 
    """
    a, b = 0., 13. # (Nondimensional) Time interval for one
    alpha = 1. / 3 # Nondimensional parameter
    dim = 2 # dimension of the system
    y0 = np.array([1/2.,3/4.]) # initial conditions
    
    # Note: swapping order of arguments to match the calling convention
    # used in the built in IVP solver.
    def Lotka_Volterra(y, x):
        return np.array([y[0] * (1. - y[1]), alpha * y[1] * (y[0] - 1.)])
    
    subintervals = 200
   
    def plotsolve(y0):
        # Using the built in ode solver
        Y = odeint(Lotka_Volterra, y0, np.linspace(a, b, subintervals))

        # Plot the direction field
        Y1, Y2 = np.meshgrid(np.arange(0, 4.5, .2), np.arange(0, 4.5, .2), sparse=True, copy=False)
        U, V = Lotka_Volterra((Y1, Y2), 0)

        Q = plt.quiver(Y1[::3, ::3], Y2[::3, ::3], U[::3, ::3], V[::3, ::3], pivot='mid', color='b', units='dots',width=3.)
        # Plot the solution in phase space
        plt.plot(Y[:,0], Y[:,1], '-k', linewidth=2.0)
        plt.plot(Y[::10,0], Y[::10,1], '*b')
    
    for i in (y0, np.array([1/16.,3/4.]), np.array([1/40.,3/4.])):
        plotsolve(i)
    
    # Plot the 2 Equilibrium points
    plt.plot(1, 1, 'ok', markersize=8)
    plt.plot(0, 0, 'ok', markersize=8)
    
    
    plt.axis([-.5, 4.5, -.5, 4.5])
    plt.title("Phase Portrait of the Lotka-Volterra Predator-Prey Model")
    plt.xlabel('Prey',fontsize=15)
    plt.ylabel('Predators',fontsize=15)
    plt.show()


def prob4():
    """ Using the RK4 method, approximate the predator-prey trajectories.
    Plot and show the solutions. Create two plots, one for each pair of 
    parameter values alpha, beta. 
    """
    def alphabeta(alpha, beta):
        a, b = 0., 13. # (Nondim
        #alph = 1. / 3 # Nondimensional parameter
        dim = 2 # dimension of the system

        # Note: swapping order of arguments to match the calling convention
        # used in the built in IVP solver.
        def Lotka_Volterra(y, x, alpha=alpha, beta=beta):
            # dU = U(1-V)
            # dV = alph*V(U-1)
            return np.array([y[0]*(1.-y[0]-y[1]), alpha*y[1]*(y[0] - beta)])

        subintervals = 200

        def plotsolve(y0):
            # Using the built in ode solver
            Y = odeint(Lotka_Volterra, y0, np.linspace(a, b, subintervals))

            # Plot the direction field
            Y1, Y2 = np.meshgrid(np.arange(0, 4.5, .2), np.arange(0, 4.5, .2), sparse=True, copy=False)
            U, V = Lotka_Volterra((Y1, Y2), 0)

            Q = plt.quiver(Y1[::3, ::3], Y2[::3, ::3], U[::3, ::3], V[::3, ::3], pivot='mid', color='b', units='dots',width=3.)
            # Plot the solution in phase space
            plt.plot(Y[:,0], Y[:,1], '-k', linewidth=2.0)
            plt.plot(Y[::10,0], Y[::10,1], '*b')

        for i in (np.array([1/3.,1/3.]), np.array([1/2., 1/5.])):
            plotsolve(i)
    
    alphabeta(1, 0.3)
    
    
    # Plot the 2 Equilibrium points
    plt.plot(1, 1, 'ok', markersize=8)
    plt.plot(0, 0, 'ok', markersize=8)
    
    
    plt.axis([-.5, 4.5, -.5, 4.5])
    plt.title("Phase Portrait of the Lotka-Volterra Predator-Prey Model")
    plt.xlabel('Prey',fontsize=15)
    plt.ylabel('Predators',fontsize=15)
    plt.show()
    
    alphabeta(1, 1.1)
    
    # Plot the 2 Equilibrium points
    plt.plot(1, 1, 'ok', markersize=8)
    plt.plot(0, 0, 'ok', markersize=8)
    
    
    plt.axis([-.5, 4.5, -.5, 4.5])
    plt.title("Phase Portrait of the Lotka-Volterra Predator-Prey Model")
    plt.xlabel('Prey',fontsize=15)
    plt.ylabel('Predators',fontsize=15)
    plt.show()

