import sqlite3 as sql
import csv

def prob1():
    db = sql.connect("prob1")
    cur = db.cursor()
    cur.execute('CREATE TABLE MajorInfo (MajorCode INT, MajorName TEXT);')
    cur.execute('CREATE TABLE CourseInfo (CourseID INT, CourseName TEXT);')

def prob2():
    db = sql.connect("prob2")
    cur = db.cursor()
    with open('icd9.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.execute('CREATE TABLE PatientInfo (PatientID INT, Gender TEXT, Age INT, ICDcode TEXT);')
    cur.executemany("INSERT INTO PatientInfo VALUES (?,?,?,?);", rows)
    
def prob3():
    db = sql.connect("prob3")
    cur = db.cursor()
    cur.execute('CREATE TABLE student_information(StudentID INT, StudentName TEXT, MajorCode INT);')
    cur.execute('CREATE TABLE major_information(MajorCode INT, MajorName TEXT);')
    cur.execute('CREATE TABLE grade_information(StudentID INT, ClassID INT, Grade TEXT);')
    cur.execute('CREATE TABLE class_information(ClassID INT, ClassName TEXT);')
    
    studentrows = [(401767594, 'Michelle Fernandez', 1),
                   (678665086, 'Gilbert Chapman', 1),
                   (553725811, 'Roberta Cook', 2),
                   (886308195, 'Rene Cross', 3),
                   (103066521, 'Cameron Kim', 4),
                   (821568627, 'Mercedes Hall', 3),
                   (206208438, 'Kristopher Tran', 2),
                   (341324754, 'Cassandra Holland', 1),
                   (262019426, 'Alfonso Phelps', 3),
                   (622665098, 'Sammy Burke', 2)];
    cur.executemany("INSERT INTO student_information VALUES (?,?,?);", studentrows)
    
    majorrows = [(1,'Math'),(2,'Science'),(3,'Writing'),(4,'Art')]
    cur.executemany("INSERT INTO major_information VALUES(?,?);", majorrows)

    graderows = [(401767594, 4, 'C'),
                (401767594, 3, 'B-'),
                (678665086, 4, 'A+'),
                (678665086, 3, 'A+'),
                (553725811, 2, 'C'),
                (678665086, 1, 'B'),
                (886308195, 1, 'A'),
                (103066521, 2, 'C'),
                (103066521, 3, 'C-'),
                (821568627, 4, 'D'),
                (821568627, 2, 'A+'),
                (821568627, 1, 'B'),
                (206208438, 2, 'A'),
                (206208438, 1, 'C+'),
                (341324754, 2, 'D-'),
                (341324754, 1, 'A-'),
                (103066521, 4, 'A'),
                (262019426, 2, 'B'),
                (262019426, 3, 'C'),
                (622665098, 1, 'A'),
                (622665098, 2, 'A')];
    cur.executemany("INSERT INTO grade_information VALUES(?,?,?);", graderows)
    
    classrows = [(1,'Calculus'),(2,'English'),(3,'Pottery'),(4,'History')]
    cur.executemany("INSERT INTO class_information VALUES(?,?);", classrows)
    
    cur.execute("SELECT * from student_information;")
    print cur.fetchall()
    
    cur.execute("SELECT * from major_information;")
    print cur.fetchall()
    
    cur.execute("SELECT * from grade_information;")
    print cur.fetchall()
    
    cur.execute("SELECT * from class_information;")
    print cur.fetchall()

def prob4():
    db = sql.connect("prob4")
    cur = db.cursor()
    with open('icd9.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
    cur.execute("DROP TABLE IF EXISTS patientinfo")
    cur.execute('CREATE TABLE patientinfo (PatientID INT, Gender TEXT, Age INT, ICDcode TEXT);')
    cur.executemany("INSERT INTO patientinfo VALUES (?,?,?,?);", rows)
    
    cur.execute('SELECT Gender,COUNT(*) FROM patientinfo WHERE (Age<=35 AND Age>=25) GROUP BY Gender;')
    print cur.fetchall()