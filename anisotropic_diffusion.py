
# coding: utf-8

# In[8]:

# Call your solutions file anisotropic_diffusion.py
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm
from scipy import linalg as la
from scipy.misc import imread, imsave
from numpy.random import random_integers, uniform, randn

# Anisotropic diffusion is a method commonly used to remove static from images. 
# For these problems, you will need to use your own images. Since we will not have much 
# time to fine-tune the algorithm, I recommend using smaller images. 
# 
# For each problem below, I will need you to put 1) the original image, and 2) the 'noisy'
# image in your git repository. Do not put the 'denoised' image in the repository; instead 
# your code should denoise and show the image. 

def anisdiff_bw_noBCs(U, N, lambda_, g):
    """ Run the Anisotropic Diffusion differencing scheme
    on the array A of grayscale values for an image.
    Perform N iterations, use the function g
    to limit diffusion across boundaries in the image.
    Operate on A inplace. """
    for i in xrange(N):
        U[1:-1,1:-1] += lambda_ *         (g(U[:-2,1:-1] - U[1:-1,1:-1]) *
        (U[:-2,1:-1] - U[1:-1,1:-1]) +
        g(U[2:,1:-1] - U[1:-1,1:-1]) *
        (U[2:,1:-1] - U[1:-1,1:-1]) +
        g(U[1:-1,:-2] - U[1:-1,1:-1]) *
        (U[1:-1,:-2] - U[1:-1,1:-1]) +
        g(U[1:-1,2:] - U[1:-1,1:-1]) *
        (U[1:-1,2:] - U[1:-1,1:-1]))
        
def anisdiff_bw_withBCs(U, N, lambda_, g):
    """ Run the Anisotropic Diffusion differencing scheme
    on the array U of grayscale values for an image.
    Perform N iterations, use the function g
    to limit diffusion across boundaries in the image.
    Operate on U inplace. """
    difs = np.empty_like(U)
    for i in xrange(N):
        difs[:-1] = g(U[1:] - U[:-1]) * (U[1:] - U[:-1])
        difs[-1] = 0
        difs[1:] += g(U[:-1] - U[1:]) * (U[:-1] - U[1:])
        difs[:,:-1] += g(U[:,1:] - U[:,:-1]) * (U[:,1:] - U[:,:-1])
        difs[:,1:] += g(U[:,:-1] - U[:,1:]) * (U[:,:-1] - U[:,1:])
        difs *= lambda_
        U += difs
        
def add_noise(imagename,changed_pixels=10000):
    # Adds noise to a black and white image
    # Read the image file imagename.
    # Multiply by 1. / 255 to change the values so that they are floating point
    # numbers ranging from 0 to 1.
    IM = imread(imagename, flatten=True) * (1. / 255)
    IM_x, IM_y = IM.shape
    
    for lost in xrange(changed_pixels):
        x_,y_ = random_integers(1,IM_x-2), random_integers(1,IM_y-2)
        val =  .25*randn() + .5
        IM[x_,y_] = max( min(val+ IM[x_,y_],1.), 0.)
    imsave(name=("noised_"+imagename),arr=IM)
    
def add_noise_color(imagename,changed_pixels=10000):
    # Adds noise to a black and white image
    # Read the image file imagename.
    # Multiply by 1. / 255 to change the values so that they are floating point
    # numbers ranging from 0 to 1.
    IM = imread(imagename) * (1. / 255)
    IM_x, IM_y, IM_z = IM.shape
    
    for lost in xrange(changed_pixels):
        x_,y_,z_ = random_integers(1,IM_x-2), random_integers(1,IM_y-2), random_integers(1,IM_z-2)
        val =  .25*randn() + .5
        IM[x_,y_,z_] = max( min(val+ IM[x_,y_,z_],1.), 0.)
    imsave(name=("noised_"+imagename),arr=IM)
    

def prob1():
    # Denoise your image, and show your results.
    add_noise("deadlift.jpg")
    U = imread("noised_deadlift.jpg",flatten=True)/255.
    plt.title("Noisy")
    plt.imshow(U, cmap=cm.gray)
    plt.show()
    
    sigma = 0.7
    lambd = 0.2
    g = lambda x: np.exp(-(x/sigma)**2)
    anisdiff_bw_noBCs(U,5,lambd,g)
    
    plt.title("Cleaned")
    plt.imshow(U, cmap=cm.gray)
    plt.show()


def prob2():
    # Denoise your image, and show your results.
    add_noise_color('deadlift.jpg')
    U = imread("noised_deadlift.jpg")/255.
    plt.title("Noisy Color")
    plt.imshow(U)
    plt.show()
    
    sigma = 0.7
    lambd = 0.2
    g = lambda x: np.exp(-(x/sigma)**2)
    anisdiff_bw_withBCs(U,5,lambd,g)
    
    plt.title("Cleaned Color")
    plt.imshow(U)
    plt.show()

prob1()
prob2()


# In[ ]:



