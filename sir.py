# Name your solution file sir.py
from __future__ import division
import numpy as np 
from scipy.integrate import ode
from pybvp6c.bvp6c import bvp6c, bvpinit, deval
from pybvp6c.structure_variable import struct
from math import cos, sin, tan,pi
import matplotlib.pyplot as plt 

# IVP:
#     dS/dt = -beta*IS
#     dI/dt = beta*IS - gamma*I
#     dR/dt = gamma*I
#     S(0) = 1 - 6.25*10e-7
#     I(0) = 6.25*10e-7
#     R(0) = 0
# beta: avg. number of contacts made per day
# gamma: reciprocal of time spent in infectious phase

def my_ode(t, y, beta, gamma):
    S, I, R = y[0], y[1], y[2]
    return np.array([-beta*I*S, beta*I*S - gamma*I, gamma*I])

def prob1(n):
    # n is the number of subintervals in the interval [0,100] 
    beta = 1/2.
    gamma = 1/4.
    S0 = 1 - 6.25*10e-7
    I0 = 6.25*10e-7
    R0 = 0
    t0 = 0
    t1 = 100
    dt = 100./(n+1)
    
    S = np.empty(n+1)
    I = np.empty(n+1)
    R = np.empty(n+1)
    
    
    x = np.linspace(0,100,n+1) 
    f = lambda t, y: my_ode(t, y, beta, gamma)
    
    r = ode(f).set_integrator('dopri5')
    r.set_initial_value(np.array([S0, I0, R0]), t0)
    
    i = 0
    while r.successful() and r.t < t1-dt:
        vals = r.integrate(r.t+dt)
        S[i] = vals[0]
        I[i] = vals[1]
        R[i] = vals[2]
        i += 1
    
    #plt.plot(x, S, x, I, x, R)
    #plt.show()
    
    # S, I, R = the solutions at the grid points in x, in one dimensional ndarrays. 
    
    return x, S, I, R 


def prob2():
    # Plot and show the solution: S, I, and R, on the interval [0,50]. 
    # Answer the two questions asked in the lab, and return them in 
    # variables three and seven. 
    n = 200
    beta = 1.
    gamma = 1/3.
    S0 = 1 - 5./3e6
    I0 = 5./3e6
    R0 = 0.
    t0 = 0
    t1 = 50
    dt = 1.*t1/(n+1)
    
    S = np.empty(n+1)
    I = np.empty(n+1)
    R = np.empty(n+1)
    x = np.linspace(0,100,n+1)
    
    f = lambda t, y: my_ode(t, y, beta, gamma)
    
    r = ode(f).set_integrator('dopri5')
    r.set_initial_value(np.array([S0, I0, R0]), t0)
    
    i = 0
    while r.successful() and r.t < t1-dt:
        vals = r.integrate(r.t+dt)
        S[i] = vals[0]
        I[i] = vals[1]
        R[i] = vals[2]
        i += 1
        
    plt.plot(x, S, x, I, x, R)
    plt.show()
    
    productive3 = S+R
    three = np.min(productive3)
    
    gamma = 1/7.
    
    f = lambda t, y: my_ode(t, y, beta, gamma)
    
    r = ode(f).set_integrator('dopri5')
    r.set_initial_value(np.array([S0, I0, R0]), t0)
    
    i = 0
    while r.successful() and r.t < t1-dt:
        vals = r.integrate(r.t+dt)
        S[i] = vals[0]
        I[i] = vals[1]
        R[i] = vals[2]
        i += 1
        
    plt.plot(x, S, x, I, x, R)
    plt.show()
    
    productive7 = S+R
    seven = np.min(productive7)
    
    #plt.plot(x, productive3, x, productive7)
    #plt.show()
    #print int(three*3e6), int(seven*3e6)
    
    return int(three*3e6), int(seven*3e6)


def prob3():
    n = 200
    beta = .3
    gamma = 1/4.
    S0 = 1 - 5./3e6
    I0 = 5./3e6
    R0 = 0.
    t0 = 0
    t1 = 400
    dt = 1.*t1/(n+1)
    
    S = np.empty(n+1)
    I = np.empty(n+1)
    R = np.empty(n+1)
    x = np.linspace(0,t1,n+1)
    
    f = lambda t, y: my_ode(t, y, beta, gamma)
    
    r = ode(f).set_integrator('dopri5')
    r.set_initial_value(np.array([S0, I0, R0]), t0)
    
    i = 0
    while r.successful() and r.t < t1-dt:
        vals = r.integrate(r.t+dt)
        S[i] = vals[0]
        I[i] = vals[1]
        R[i] = vals[2]
        i += 1
        
    plt.plot(x, S, x, I, x, R)
    plt.show()

# IVP for SEIR model:
#     dS/dt = mu - beta(t)*I*S
#     dE/dt = beta(t)*I*S - E/lambda
#     dI/dt = E/lambda - I/eta

def prob4():
    beta0 = 1575
    beta1 = 1
    beta = lambda t: beta0*(1+beta1*cos(2*np.pi*t))
    eta = 0.01
    lambd = 0.0279
    mu = 0.02
    # Time in years; run on [0,1] to show a one-year cycle
    # ------------------------------------------------------------
    def ode(t,y):
        S, E, I = y[0], y[1], y[2]
        return np.array([mu - beta(t)*I*S, 
                         beta(t)*I*S - E/lambd, 
                         E/lambd - I/eta,
                         0,
                         0,
                         0]);
                           
    # ------------------------------------------------------------
    def f_jacobian(t,y):
        S, E, I = y[0], y[1], y[2]
        return np.array([[-beta(t)*I, 0, -beta(t)*S, 0, 0, 0], 
                         [beta(t)*I, -1/lambd, beta(t)*S, 0, 0, 0], 
                         [0, 1/lambd, -1/eta, 0, 0, 0], 
                         [0,0,0,0,0,0], 
                         [0,0,0,0,0,0], 
                         [0,0,0,0,0,0]]);
    
                          
    # ------------------------------------------------------------
    def bc_jacobian(x,y):
        dGdya = np.array([ [1, 0, 0,-1, 0, 0],
                           [0, 1, 0, 0,-1, 0], 
                           [0, 0, 1, 0, 0,-1],
                           [0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0]]);
        
        dGdyb = np.array([ [0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0],
                           [1, 0, 0,-1, 0, 0],
                           [0, 1, 0, 0,-1, 0],
                           [0, 0, 1, 0, 0,-1]]);
        return dGdya,dGdyb

    # ------------------------------------------------------------
    def bcs(ya,yb):
        return np.array([ya[0] - ya[3], 
                         ya[1] - ya[4], 
                         ya[2] - ya[5], 
                         yb[0] - yb[3],
                         yb[1] - yb[4],
                         yb[2] - yb[5]]);

    # ------------------------------------------------------------
    def init(x):
        S = .1 + .05 * cos(2. * pi * x)
        return np.array([.65+.4*S,.05*(1.-S)-.042,.05*(1.-S)-.042,.075,.005,.005])

    # ------------------------------------------------------------
    options = struct()
    # options include abstol, reltol, singularterm, stats, vectorized, maxnewpts,slopeout,xint
    options.abstol, options.reltol = 1e-8, 1e-7
    options.fjacobian, options.bcjacobian = f_jacobian, bc_jacobian
    options.nmax = 20000

    solinit = bvpinit(np.linspace(0,1,100),init)
    sol = bvp6c(ode,bcs,solinit,options)
    
    xint = np.linspace(0,1,100); Sxint,_ = deval(sol,xint)

    for j in range(3):
        plt.plot(sol.x,sol.y[j],linewidth=2.0)
    # plt.axis([-0.02, 1.02, .95, 1.8])
    plt.title('Numerical Solution')
    plt.xlabel('x'); plt.ylabel('y'); plt.show()
    
    C = sol.y[0][3:]
    return C