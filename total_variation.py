
# coding: utf-8

# In[5]:

import numpy as np
import numexpr as ne

from scipy.misc import imread, imsave
from numpy.random import random_integers, uniform, randn
from matplotlib import pyplot as plt
from matplotlib import cm


# Call your solutions file total_variation.py

# Total Variation is a method commonly used to remove static from images. 
# For these problems, you will need to use your own images. Since we will not have much 
# time to fine-tune the algorithm, I recommend using smaller images. 
# 
# For each problem below, I will need you to put 1) the original image, and 2) the 'noisy'
# image in your git repository. Do not put the 'denoised' image in the repository; instead 
# your code should denoise and show the image. 

# The code below can be used to add noise to your images. 


def add_noise(imagename,changed_pixels=10000):
    # Adds noise to a black and white image
    # Read the image file imagename.
    # Multiply by 1. / 255 to change the values so that they are floating point
    # numbers ranging from 0 to 1.
    IM = imread(imagename, flatten=True) * (1. / 255)
    IM_x, IM_y = IM.shape

    for lost in xrange(changed_pixels):
        x_,y_ = random_integers(1,IM_x-2), random_integers(1,IM_y-2)
        val =  .25*randn() + .5
        IM[x_,y_] = max( min(val+ IM[x_,y_],1.), 0.)
    imsave(name=("noised_"+imagename),arr=IM)


def prob1(filename='deadlift.jpg',steps=250):
    # Denoise your (black and white) image.

    # Show the noisy image, and the denoised images. 

    add_noise(filename)
    f = imread('noised_'+filename, flatten=True)/255.
    x, y = f.shape
    u = np.empty((2,x,y))
    dt = 1e-3
    lambd = 40
    u = f
    
    for i in xrange(250):
        u_xx = np.roll(u,-1,axis=0) - 2*u + np.roll(u,1,axis=0)
        u_yy = np.roll(u,-1,axis=1) - 2*u + np.roll(u,1,axis=1)
        lap = u_xx + u_yy
        u = u - dt*(u - f - lambd*lap)
        
    plt.subplot(211)
    plt.imshow(f, cmap=cm.Greys_r)
    plt.title("Noisy")
    plt.subplot(212)
    plt.imshow(u, cmap=cm.Greys_r)
    plt.title("Clean")
    plt.show()


def prob2(filename='deadlift.jpg'):
    # Denoise your (black and white) image.

    # Show the noisy image, and the denoised images. 

    add_noise(filename)
    f = imread('noised_'+filename, flatten=True)/255.
    x, y = f.shape
    u = np.empty((2,x,y))
    eps = 1e-5
    dt = 1e-3
    lambd = 1
    u = f
    
    for i in xrange(250):
        u_xx = np.roll(u,-1,axis=0) - 2*u + np.roll(u,1,axis=0)
        u_yy = np.roll(u,-1,axis=1) - 2*u + np.roll(u,1,axis=1)
        
        u_x = 0.5*(np.roll(u,-1,axis=0) - np.roll(u,1,axis=0))
        u_y = 0.5*(np.roll(u,-1,axis=1) - np.roll(u,1,axis=1))
        
        u_xy = 0.5*(np.roll(u_x,-1,axis=1) - np.roll(u_x,1,axis=1))
        
        lap = u_xx + u_yy
        u = u + dt*(-lambd*(u-f) + (u_xx*u_y**2+u_yy*u_x**2-2*u_x*u_y*u_xy)/(u_x**2+u_y**2+eps)**(3/2))
    
    plt.subplot(211)
    plt.imshow(f, cmap=cm.Greys_r)
    plt.title("Noisy")
    plt.subplot(212)
    plt.imshow(u, cmap=cm.Greys_r)
    plt.title("Clean")
    plt.show()  


# In[ ]:



