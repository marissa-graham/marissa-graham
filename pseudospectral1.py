# Call this lab pseudospectral1.py
import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as interp
from scipy import linalg as la

def cheb(N):
    x = np.cos((np.pi/N)*np.linspace(0,N,N+1))
    x.shape = (N+1,1)
    lin = np.linspace(0,N,N+1)
    lin.shape = (N+1,1)
    c = np.ones((N+1,1))
    c[0], c[-1] = 2., 2.
    c = c*(-1.)**lin
    X = x*np.ones(N+1) # broadcast along 2nd dimension (columns)
    dX = X - X.T
    D = (c*(1./c).T)/(dX + np.eye(N+1))
    D = D - np.diag(np.sum(D.T,axis=0))
    x.shape = (N+1,)
    # Here we return the differentiation matrix and the Chebyshev points,
    # numbered from x_0 = 1 to x_N = -1
    return D, x

def prob1():
    """
    Plot your approximation
    """
    f = lambda x:np.exp(x)*np.cos(6.0*x)
    fprime = lambda x:-6.0*np.exp(x)*np.sin(6.0*x)+np.exp(x)*np.cos(6.0*x)
    d = np.linspace(-1,1,100)
    
    for N in [6,8,10]:
        D, x = cheb(N)
        #x = x[::-1]
        y = interp.barycentric_interpolate(x,np.dot(D,f(x)),d)
        plt.plot(d,y,label="N="+str(N))
    
    plt.plot(d,fprime(d),label="Actual")
    plt.legend()
    plt.show()



def prob2():
    """ 
    Plot the true solution, and your numerical solution. Choose a suitable number
    of Chebychev points for the pseudospectral method.

    """
    c1 = -0.125*(np.exp(2.)-np.exp(-2.))
    c0 = 0.25*np.exp(-2.) + c1
    u = lambda x:0.25*np.exp(2.*x) + c0*x + c1
    
    d = np.linspace(-1,1,100)
    D, x = cheb(15)
    f = np.exp(2.*x)
    f[0], f[-1] = 0., 0.
    D2 = np.dot(D,D)
    D2[0,1:] = 0.
    D2[0,0] = 1.
    D2[-1,:-1] = 0.
    D2[-1,-1] = 1.
    soln = la.solve(D2,f)
    y = interp.barycentric_interpolate(x,soln,d)
    
    plt.plot(d, y,label='Approx')
    plt.plot(d,u(d),label='Actual')
    plt.legend()
    plt.show()

def prob3():
    """ 
    Plot and show your numerical solution. Choose a suitable number
    of Chebychev points for the pseudospectral method.

    """
    d = np.linspace(-1,1,100)
    D, x = cheb(10)
    f = np.exp(3.*x)
    f[0], f[-1] = 2., -1.
    
    D = np.dot(D,D) + D
    D[0,1:] = 0.
    D[0,0] = 2.
    D[-1,:-1] = 0.
    D[-1,-1] = -1.
    soln = la.solve(D,f)
    y = interp.barycentric_interpolate(x,soln,d)
    
    plt.plot(d, y)
    plt.show()


def prob4():
    """ 
    Plot and show your numerical solution. Choose a suitable number
    of Chebychev points for the pseudospectral method.

    """
    pass


def prob5():
    """ 
    Plot and show your numerical solution. On a separate plot, graph the minimizing surface. 
    return chebychev_grid, numerical_solution

    """
    pass
