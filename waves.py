# Call your solutions file waves.py
import numpy as np
from matplotlib import animation
from matplotlib import pyplot as plt
from scipy import linalg as la


def prob1():
    # For the initial boundary value problem given:
    # u_tt = u_xx
    # u(0,t) = u(1,t) = 0
    # u(x,0) = sin(2*pi*x) this is f(x)
    # u_t(x,0) = 0 this is g(x)
    
    # Plot true solution: sin(2*pi*x)cos(2*pi*t)
    # Plot approx solution at t=0.5, using 5 subintervals in each direction

    # Define constants
    s_squared = 1.0
    t_del = 0.1
    n = 5
    x = np.linspace(0,1,n)
    # lambda = s*change in t / change in x
    l = s_squared*t_del/0.2
    
    # First time step
    U = np.zeros((n,5),dtype=np.float)
    U[:,0] = np.sin(2*np.pi*x)
    U[0,0] = 0
    
    # Second time step
    U[:,1] = U[:,0] + s_squared*-4*np.pi**2*np.sin(2*np.pi*x)*t_del**2/2
    U[0,1] = 0
    
    A = np.diag(2*(1-l**2)*np.ones(n))+np.diag(l**2*np.ones(n-1),-1)+np.diag(l**2*np.ones(n-1),1)
    
    for i in xrange(2,5):
        U[:,i] = np.dot(A,U[:,i-1])-U[:,i-2]
        U[0,i] = 0
    
    plt.plot(x,U[:,4])
    dom = np.linspace(0,1,50)
    plt.plot(dom,np.sin(2*np.pi*dom)*np.cos(2*np.pi*.5))
    plt.show()
    


def prob2():
    # Produce two animations using the given discretizations in space and time. 
    # The second animation should show instability in the numerical solution, due to 
    # the CFL stability condition being broken. 

    s = 1.0
    m = 20
    n = 200
    x = np.linspace(0,1,n)
    def my_f(x):
        return 0.2*np.exp(-m**2*(x-0.5)**2)
    def g(x):
        return -.4*m**2*(x-0.5)*np.exp(-m**2*(x-0.5)**2)
    def gprime(x):
        return -2*m**2*(x-0.5)*g(x) - 2*m**2*my_f(x)
    
    for t_steps in [220,180]:
        t_del = 1.0/t_steps
        l = s*t_del*n
        
        U = np.zeros((n,t_steps),dtype=np.float)
        U[:,0] = my_f(x)
        #U[0,0] = 0
        
        # Second time step: U0 + g*t_del + s**2*f''(x)*t_del**2/2
        U[:,1] = U[:,0] + s*t_del*g(x) + gprime(x)*t_del**2/2.0
        #U[0,1] = 0

        A = np.diag(2*(1-l**2)*np.ones(n))+np.diag(l**2*np.ones(n-1),-1)+np.diag(l**2*np.ones(n-1),1)
    
        for i in xrange(2,t_steps):
            U[:,i] = np.dot(A,U[:,i-1])-U[:,i-2]
            #U[0,i] = 0
        
        f = plt.figure()
        plt.axes(xlim=(0,1),ylim=(-1,1))
        line, = plt.plot([],[])

        def animate(i):
            line.set_data(x, U[:,i])
            return line,

        a = animation.FuncAnimation(f, animate, frames=t_steps, interval=50)
        plt.show()

        
def prob3():
    # Numerically approximate the solution from t = 0 to t = 2 using the given discretization, 
    # and animate your results. 
    s = 1.0
    m = 20
    n = 200
    x = np.linspace(0,2,n)
    def my_f(x):
        return 0.2*np.exp(-m**2*(x-0.5)**2)
    def g(x):
        return 0
    def fprime(x):
        return -2*m**2*(x-0.5)*-.4*m**2*(x-0.5)*np.exp(-m**2*(x-0.5)**2) - 2*m**2*my_f(x)
    
    t_steps = 440
    t_del = 2.0/t_steps
    l = s*t_del*n

    U = np.zeros((n,t_steps),dtype=np.float)
    U[:,0] = my_f(x)
    #U[0,0] = 0

    # Second time step: U0 + g*t_del + s**2*f''(x)*t_del**2/2
    U[:,1] = U[:,0] + s*t_del*g(x) + fprime(x)*t_del**2/2.0
    #U[0,1] = 0

    A = np.diag(2*(1-l**2)*np.ones(n))+np.diag(l**2*np.ones(n-1),-1)+np.diag(l**2*np.ones(n-1),1)

    for i in xrange(2,t_steps):
        U[:,i] = np.dot(A,U[:,i-1])-U[:,i-2]
        #U[0,i] = 0

    f = plt.figure()
    plt.axes(xlim=(0,2),ylim=(-.15,.15))
    line, = plt.plot([],[])

    def animate(i):
        line.set_data(x, U[:,i])
        return line,

    a = animation.FuncAnimation(f, animate, frames=t_steps, interval=5)
    plt.show()


def prob4():
    # Numerically approximate the solution from t = 0 to t = 2 using the given discretization, 
    # and animate your results.

    s = 1.0
    m = 20
    n = 200
    x = np.linspace(0,1,n)
    def my_f(x):
        soln = np.zeros_like(x)
        soln[x<6./11.] = 1./3
        soln[x<5./11.] = 0.
        return soln
    
    def g(x):
        return 0
    
    t_steps = 440
    t_del = 2.0/t_steps
    l = s*t_del*n

    U = np.zeros((n,t_steps),dtype=np.float)
    U[:,0] = my_f(x)
    #U[0,0] = 0

    # Second time step: U0 + g*t_del + s**2*f''(x)*t_del**2/2
    U[:,1] = U[:,0] + s*t_del*g(x) - l**2*U[:,0]
    U[:-1,1] += l**2/2.*U[1:,0]
    U[1:,1] += l**2/2.*U[:-1,0]
    #U[0,1] = 0

    A = np.diag(2*(1-l**2)*np.ones(n))+np.diag(l**2*np.ones(n-1),-1)+np.diag(l**2*np.ones(n-1),1)

    for i in xrange(2,t_steps):
        U[:,i] = np.dot(A,U[:,i-1])-U[:,i-2]
        #U[0,i] = 0

    f = plt.figure()
    plt.axes(xlim=(0,1),ylim=(-.5,.5))
    line, = plt.plot([],[])

    def animate(i):
        line.set_data(x, U[:,i])
        return line,

    a = animation.FuncAnimation(f, animate, frames=t_steps, interval=5)
    plt.show()
