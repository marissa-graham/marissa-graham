
# coding: utf-8

# In[22]:

import numpy as np
from matplotlib import pyplot as plt

### Code from RK4 Lab with minor edits
def initialize_all(y0, t0, t, n):
    """ An initialization routine for the different ODE solving
    methods in the lab. This initializes Y, T, and h. """
    
    if isinstance(y0, np.ndarray):
        Y = np.empty((n, y0.size)).squeeze()
    else:
        Y = np.empty(n)
    Y[0] = y0
    T = np.linspace(t0, t, n)
    h = float(t - t0) / (n - 1)
    return Y, T, h

def RK4(f, y0, t0, t, n):
    """ Use the RK4 method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept three arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    The third is an index to the other arrays.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    
    Y,T,h = initialize_all(y0,t0,t,n)
    print "Y.shape:", Y.shape
    for i in xrange(n-1):
        print 'T[i].shape, Y[i].shape', T[i].shape, ',', Y[i].shape
        K1 = f(T[i],Y[i],i)
        K2 = f(T[i]+h/2.,Y[i]+h/2.*K1,i)
        K3 = f(T[i]+h/2.,Y[i]+h/2.*K2,i)
        K4 = f(T[i+1],Y[i]+h*K3,i)
        print K1.shape
        print K2.shape
        print K3.shape
        print K4.shape
        Y[i+1] = Y[i] + h/6.*(K1+2*K2+2*K3+K4)
    return Y

### Global variables to be used throughout the lab

# define constants
s_1 = 2.
s_2 = 1.5
mu = 0.002
k = 0.000025
g = 30.
c = 0.007
B_1 = 14
B_2 = 1

# Not specified until problem 5
n = 1000 
T_0, V_0 = 400, 3
t_f = 50

# initialize global variables, state, costate, and u.
state = np.zeros((n,2))
state0 = np.array([T_0, V_0])
costate = np.zeros((n,2))
costate0 = np.zeros(2)
u=np.zeros((n,2))
u[:,0] += .02
u[:,1] += .9

def prob1():
    print "The HIV virus is unique because it targets helper T cells     specifically, suppressing the body's system for fighting infections."
    
def prob2():
    print "AIDS is treated using drugs that suppress the virus and     bolster the immune system. Negative side effects and cost of the    drugs used make it important to minimize the amount used (while still    maximizing patient outcomes)."
    
def state_equations(t,y,i):
    '''
    Parameters
    ---------------
    t : float
    the time
    y : ndarray (2,)
    the T cell concentration and the Virus concentration at time t
    i : int
    index for the global variable u.
    Returns
    --------------
    y_dot : ndarray (2,)
    the derivative of the T cell concentration and the virus ←-
    concentration at time t
    '''
    print 'y.shape:', y.shape
    V = y[1]
    T = y[0]
    print 'V and T shapes:', V.shape, ' and ', T.shape
    
    Tprime = s_1 - s_2*V/(B_1 + V) - mu*T - k*V*T + u[i]*T
    Vprime = g*V/(B_2+V) * (1-u[i+1] - c*V*T)
    
    print 'Tprime and Vprime shapes:', Tprime.shape, ' and ', Vprime.shape
    
    return np.array([Tprime,Vprime])

def lambda_hat(t,y,i):
    '''
    Parameters
    ---------------
    t : float
    the time
    y : ndarray (2,)
    the lambda_hat values at time t
    i : int
    index for global variables, u and state.
    
    Returns
    --------------
    y_dot : ndarray (2,)
    the derivative of the lambda_hats at time t.
    '''
    
    V = state[-1-i,1]
    T = state[-1-i,0]

    L1 = y[0]
    L2 = y[1]
    
    u1 = u[i]
    u2 = u[i+1]
    
    K1 = B_1 + V**2
    K2 = B_2 + V**2

    L1prime = L1*(-mu - k*V + u1) - c*L2*V + 1
    L2prime = -L1*(s_2*B_1/K1 + k*T) + L2*(g*B_2*(1-u2)/K2 - c*T)
    
    return np.array([L1prime, L2prime])

# Problem 5
def prob5():
    a_1, a_2 = 0, 0
    b_1, b_2 = 0.02, 0.9
    s_1, s_2 = 2., 1.5
    mu = 0.002
    k = 0.000025
    g = 30.
    c = 0.007
    B_1, B_2 = 14, 1
    A_1, A_2 = 250000, 75
    T_0, V_0 = 400, 3
    t_f = 50
    n = 1000
    
    epsilon = 0.001
    test = epsilon + 1
    
    while(test > epsilon):
        oldu = u.copy();
        
        #solve the state equations with forward iteration
        #f1 = state_equations(0,np.array([T_0,V_0]),1)
        state = RK4(state_equations,np.array([T_0,V_0]),0,t_f,n)
    
        #solve the costate equations with backwards iteration
        #f2 = lambda_hat(0,np.array([0,0]),1,np.array([T_0,V_0]))
        costate = RK4(lambda_hat,np.array([0,0]),t_f,0,n)[::-1]
    
        #solve for u1 and u2
        #u1 = min(max(a_1,(1/(2*A_1))*costate[0]*state[0]),b_1)
        #u2 = min(max(a_2,-1*(costate[1]/(2*A_2))*((g*state[1])/(B_2+state[1])),b_2))
        #solve for u1
        a1array = np.ones(n) * a_1
        b1array = np.ones(n) * b_1
        ustar = 1 / (2. * A_1) * (costate[:,0] * state[:,0])
        maxu1 = np.maximum(a1array, ustar)
        u1 = np.minimum(b1array, maxu1)

        #solve for u2
        a2array = np.ones(n) * a_2
        a2array = np.ones(n) * a_2
        b2array = np.ones(n) * b_2
        u2star = 1 / (2. * A_2) * (-1 * costate[:,1] * g * state[:,1]) / (B_2 + state[:,1])
        maxu2 = np.maximum(a2array, u2star)  
        u2 = np.minimum(b2array, maxu2)
        
        #update control
        u[:,0] = 0.5*(u1 + oldu[:,0])
        u[:,1] = 0.5*(u2 + oldu[:,1])
        
        #test for convergence
        test = abs(oldu - u).sum()
    
    plt.subplot(221)
    plt.plot(u[:,0])
    plt.title("u1")

    plt.subplot(222)
    plt.plot(u[:,1])
    plt.title("u2")
    
    plt.subplot(223)
    plt.plot(f2[:,0])
    plt.title("T")
    
    plt.subplot(224)
    plt.plot(f2[:,1])
    plt.title("V")
    
    plt.show()
    
prob5()


# In[ ]:



