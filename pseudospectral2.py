# Call this lab pseudospectral2.py
from __future__ import division
import numpy as np
from scipy.fftpack import fft, ifft
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.integrate import odeint


def deriv_approx(f,k):
    # Approximates the derivative using the pseudospectral method
    f_hat = fft(f)
    fp_hat = ((1j*k)*f_hat)
    fp = np.real(ifft(fp_hat))
    return fp

def prob1(N):
    """
    N = integer
    Define u(x) = np.sin(x)**2.*np.cos(x) + np.exp(2*np.sin(x+1)) .
    Find the Fourier approximation for .5 uâ€™â€™-uâ€™ at the 
    Fourier grid points for a given N.
    Plot the true solution, along with your numerical solution.
    """
    N=24
    x1 = (2.*np.pi/N)*np.arange(1,N+1)
    f = np.sin(x1)**2.*np.cos(x1) + np.exp(2.*np.sin(x1+1))
    k = np.concatenate(( np.arange(0,N/2),np.array([0]),np.arange(-N/2+1,0,1)))

    dprox = deriv_approx(f,k)
    d2prox = deriv_approx(dprox,k)
    
    # Calculates the derivative analytically
    x2 = np.linspace(0,2*np.pi,200)
    d1 = (2.*np.sin(x2)*np.cos(x2)**2.-np.sin(x2)**3.+2*np.cos(x2+1)*np.exp(2*np.sin(x2+1)))
    d2 = -2.*np.exp(2.*np.sin(x2+1))*np.sin(x2+1) + 2.*np.cos(x2)*(np.cos(x2)**2-np.sin(x2)**2) + 4.*np.exp(2.*np.sin(x2+1))*np.cos(x2+1)**2 - 5.*np.sin(x2)**2*np.cos(x2)
    plt.plot(x2,0.5*d2 - d1,'-k',linewidth=1.)
    plt.plot(x1,0.5*d2prox - dprox, '*b')
    plt.show()
    
def initialize_all(y0, t0, t, n):
    """ An initialization routine for the different ODE solving
    methods in the lab. This initializes Y, T, and h. """
    if isinstance(y0, np.ndarray):
        Y = np.empty((n, y0.size)).squeeze()
    else:
        Y = np.empty(n)
    Y[0] = y0
    T = np.linspace(t0, t, n)
    h = float(t - t0) / (n - 1)
    return Y, T, h

    
def RK4(f, y0, t0, t, n):
    """ Use the RK4 method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept two arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    Y, T, h = initialize_all(y0, t0, t, n)
    for i in xrange(n-1):
        k1 = f(T[i] ,Y[i])
        k2 = f(T[i] + h/2, Y[i] + h*k1/2)
        k3 = f(T[i] + h/2, Y[i] + h*k2/2)
        k4 = f(T[i+1], Y[i] + h*k3)
        Y[i+1] = Y[i] + h/6*(k1 + 2*k2 + 2*k3 + k4)
    return Y

def prob2():
    """ 
    Using a fourth order Runge-Kutta method (RK4), solve the initial 
    value problem u_t + c(x)u_x = 0, where c(x) = .2 + sin^2(x − 1), and 
    u(x, t = 0) = e^(−100(x−1)^2). Plot your numerical solution from 
    t = 0 to t = 8. Note that the initial data is nearly zero near x = 0 
    and 2π, and so we can use the pseudospectral method.
    
    Create the 3d plot of the numerical solution from t=0 to t=8. 

    """
    
    # Get the Fourier grid points
    N = 200
    x = np.linspace(0,2.0*np.pi,N)
    c = 0.2 + np.sin(x-1)**2
    y0 = np.exp(-100.*(x-1)**2)
    
    k = np.zeros(N)
    for i in xrange(1,N+1):
        k[i-1] = -N/2 + i
        
    k0 = np.concatenate((np.arange(0,N/2),np.array([0]),np.arange(-N/2+1,0,1)))
        
    def f(y,t):
        return np.real(-c*ifft((1j*k0)*fft(y)))
    
    # Get -c(x)F^-1(ikF(U)); U is the u(x,t=0) at the Fourier grid pts
    #blob = -c(x)*ifft(1j*k*fft(u0(x)))
    
    # Now you can call the RK4 method on that thing
    #sol = RK4(f, y0, 0, 8, N)
    sol = odeint(f,y0,np.linspace(0,8,N))
    
    # Plot the thing
    X, Y = np.meshgrid(x,np.linspace(0,8,N))
    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_surface(X,Y,sol)
    plt.show()

prob2()

