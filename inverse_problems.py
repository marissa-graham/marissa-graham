# Call this lab inverse_problems.py
import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as plt
%matplotlib inline

def prob1(n):
    """
    n = number of subintervals for the domain [0,1].

    Plot your approximation for a(x) at the grid points 
    x = np.linspace(0,1,n+1)
    """
    c0 = 3/8.
    c1 = 1.25
    
    def f(x):
        out = -np.ones(x.shape)
        m = np.where(x<.5)
        out[m] = -6*x[m]**2. + 3.*x[m] - 1.
        return out

    def u(x):
        return (x+1./4)**2. + 1./4

    def integral_of_f(x):
        # out = \int_0^x f(s) ds
        m = np.where(x>0.5)
        notm = np.where(x<=0.5)
        
        out = np.zeros_like(x)
        out[notm] = -0.5*x[notm]*(4.*x[notm]**2-3.*x[notm]+2.)
        out[m] = -0.375 + -1*(x[m]-0.5)
        return out

    def derivative_of_u(x):
        # out = u'(x)
        out = 2*x + 0.5
        return out

    x = np.linspace(0,1,n+1)
    F, u_p = integral_of_f(x), derivative_of_u(x)

    def sum_of_squares(alpha):
        return np.sum(((c0-F)/alpha - u_p)**2)

    guess = (1./4)*(3-x)
    sol = minimize(sum_of_squares,guess)
    plt.plot(x,sol.x,'-ob',linewidth=2)
    plt.show()

# -(au')' = f,      x in (0,1)
# a(0)u'(0) = c0,   a(1)u'(1) = c1


def prob2(n,epsilon):
    """
    n = number of subintervals for the domain [0,1].
    epsilon is some parameter value > 0.6605

    Compute your approximation for a(x) at the grid points 
    x = np.linspace(0,1,n+1)

    return x, a(x)
    """
    c0 = 1.0
    c1 = 2.0
    
    def f(x):
        out = -np.ones(x.shape)
        return out

    def u(x):
        return x + 1 + epsilon*np.sin(x/epsilon**2)

    def integral_of_f(x):
        return -1*x

    def derivative_of_u(x):
        # out = u'(x)
        out = 1+np.cos(x/epsilon**2)/epsilon
        return out

    x = np.linspace(0,1,n+1)
    F, u_p = integral_of_f(x), derivative_of_u(x)

    def sum_of_squares(alpha):
        return np.sum(((c0-F)/alpha - u_p)**2)

    guess = (1./4)*(3-x)
    sol = minimize(sum_of_squares,guess)
    plt.plot(x,sol.x,'-ob',linewidth=2)
    plt.show()

#for epsilon in [0.66049145, 0.661, 0.7, 1., 50., 100.]:
#    prob2(50,epsilon)